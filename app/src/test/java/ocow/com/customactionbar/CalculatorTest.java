package ocow.com.customactionbar;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {
    @Test
    public void relativeAbundance1() throws Exception {
        System.out.println("Test : Relative Abundance 1");
        Calculator calculator = new Calculator();
        int result = calculator.relativeAbundance(23);
        int expResult = 3;
        assertEquals(expResult, result);
    }

    @Test
    public void relativeAbundance2() throws Exception {
        System.out.println("Test : Relative Abundance 2");
        Calculator calculator = new Calculator();
        int result = calculator.relativeAbundance(101);
        int expResult = 5;
        assertEquals(expResult, result);
    }

    @Test
    public void group1Score1() throws Exception {
        System.out.println("Test : Group 1 Score 1");
        Calculator calculator = new Calculator();
        int result = calculator.group1Score(1, 4);
        int expResult = 6;
        assertEquals(expResult, result);
    }

    @Test
    public void group1Score2() throws Exception {
        System.out.println("Test : Group 1 Score 2");
        Calculator calculator = new Calculator();
        int result = calculator.group1Score(2, 2);
        int expResult = 4;
        assertEquals(expResult, result);
    }

    @Test
    public void group2Score1() throws Exception {
        System.out.println("Test : Group 2 Score 1");
        Calculator calculator = new Calculator();
        int result = calculator.group2Score(1, 5);
        int expResult = 6;
        assertEquals(expResult, result);
    }

    @Test
    public void group2Score2() throws Exception {
        System.out.println("Test : Group 2 Score 2");
        Calculator calculator = new Calculator();
        int result = calculator.group2Score(3, 3);
        int expResult = 8;
        assertEquals(expResult, result);
    }

    @Test
    public void group3Score1() throws Exception {
        System.out.println("Test : Group 3 Score 1");
        Calculator calculator = new Calculator();
        int result = calculator.group3Score(2, 1);
        int expResult = 2;
        assertEquals(expResult, result);
    }

    @Test
    public void group3Score2() throws Exception {
        System.out.println("Test : Group 3 Score 2");
        Calculator calculator = new Calculator();
        int result = calculator.group3Score(4, 3);
        int expResult = 6;
        assertEquals(expResult, result);
    }

    @Test
    public void group4Score1() throws Exception {
        System.out.println("Test : Group 4 Score 1");
        Calculator calculator = new Calculator();
        int result = calculator.group4Score(1, 6);
        int expResult = 2;
        assertEquals(expResult, result);
    }

    @Test
    public void group4Score2() throws Exception {
        System.out.println("Test : Group 4 Score 2");
        Calculator calculator = new Calculator();
        int result = calculator.group4Score(3, 6);
        int expResult = 4;
        assertEquals(expResult, result);
    }

    @Test
    public void group5Score1() throws Exception {
        System.out.println("Test : Group 5 Score 1");
        Calculator calculator = new Calculator();
        int result = calculator.group5Score(12);
        int expResult = 2;
        assertEquals(expResult, result);
    }

    @Test
    public void group5Score2() throws Exception {
        System.out.println("Test : Group 5 Score 2");
        Calculator calculator = new Calculator();
        int result = calculator.group5Score(24);
        int expResult = 0;
        assertEquals(expResult, result);
    }

    @Test
    public void group5Score3() throws Exception {
        System.out.println("Test : Group 5 Score 3");
        Calculator calculator = new Calculator();
        int result = calculator.group5Score(0);
        int expResult = 6;
        assertEquals(expResult, result);
    }

    @Test
    public void finalScore1() throws Exception {
        System.out.println("Test : Final Score 1");
        Calculator calculator = new Calculator();
        int result = calculator.finalScore(1, 1, 4, 1, 1);
        int expResult = 8;
        assertEquals(expResult, result);
    }

    @Test
    public void finalScore2() throws Exception {
        System.out.println("Test : Final Score 2");
        Calculator calculator = new Calculator();
        int result = calculator.finalScore(3, 1, 4, 2, 1);
        int expResult = 11;
        assertEquals(expResult, result);
    }

}
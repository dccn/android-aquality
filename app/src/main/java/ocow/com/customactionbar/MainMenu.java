package ocow.com.customactionbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.ArrayList;
import java.util.List;

public class MainMenu extends ToolbarConfig {

    private Button newSample_btn, veiwData_btn, getSocial_btn;
    private MixpanelAPI mixpanelAPI;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        firebaseAuth = FirebaseAuth.getInstance();

        toolBar();

        mixpanelAPI = MixpanelAPI.getInstance(this, "b4c3f5a97ec1f88c504719c9732f6061");
        mixpanelAPI.identify(firebaseAuth.getUid());
        mixpanelAPI.getPeople().identify(firebaseAuth.getUid());

        newSample_btn = (Button) findViewById(R.id.newSample);
        veiwData_btn = (Button) findViewById(R.id.getdata);
//        getSocial_btn = (Button) findViewById(R.id.getSocial);


        newSample_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainMenu.this, MainActivity.class);
                startActivity(in);
            }
        });

        veiwData_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create an Activity with a webview that will go to the web app.
                Intent in = new Intent(MainMenu.this, DataGathered.class);
                startActivity(in);
            }
        });
//
//        getSocial_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //Links to different social medias.
//                    //Possible a context menu...
//            }
//        });

    }

    @Override
    public void toolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.aquality_logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
    }


}

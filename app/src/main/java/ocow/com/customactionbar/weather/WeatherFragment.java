package ocow.com.customactionbar.weather;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ocow.com.customactionbar.R;

public class WeatherFragment extends Fragment {

    TextView cityField, updateField, detailsField, currentTempField, weatherField;
    Handler handler;
    ImageView weather_icon;
    private static final int MY_PERMISSION = 1;
    String TAG = "Weather Fragments";

    public WeatherFragment() {
        handler = new Handler();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        updateWeatherData(new CityPreferences(getActivity()).getCity()+",IE");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_weather, container, false);

        cityField = (TextView) rootView.findViewById(R.id.city_field);
        updateField=(TextView) rootView.findViewById(R.id.update_field);
        detailsField=(TextView) rootView.findViewById(R.id.details_field);
        currentTempField = (TextView) rootView.findViewById(R.id.current_temp_field);
//        weatherField = (TextView) rootView.findViewById(R.id.weather_field);
        weather_icon=(ImageView) rootView.findViewById(R.id.weather_icon);



        return rootView;
    }


    private void updateWeatherData (final String city) {
        new Thread(){
            @Override
            public void run() {
                Locale l = Locale.getDefault();
                final JSONObject json = RemoteFetch.getJSON(getActivity(), city);
                if(json == null){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(),getActivity().getString(R.string.place_not_found), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            renderWeather(json);
                        }
                    });
                }
            }
        }.start();
    }

    private void renderWeather(JSONObject json) {
        try {
            String t = json.getString("name").toUpperCase(Locale.ENGLISH)
                    + " " +json.getJSONObject("sys").getString("country");
            cityField.setText(t);
            JSONObject details = json.getJSONArray("weather").getJSONObject(0);
            JSONObject main = json.getJSONObject("main");
            String d = details.getString("description")
                    .toUpperCase(Locale.ENGLISH) +"\n" + "Humidity: "
                    + main.getString("humidity") +"%"+"\n"
                    +"Pressure: "+main.getString("pressure")+"hPa";
            detailsField.setText(d);
            currentTempField.setText(String.format("%.2f",main.getDouble("temp"))
                    +" °C");
            DateFormat df = DateFormat.getDateInstance();
            String updateOn=df.format(new Date(json.getLong("dt")*1000));
            updateField.setText("Last update: "+updateOn);

            setWeatherIcon(details.getInt("id"), json.getJSONObject("sys")
                    .getLong("sunrise")*1000, json.getJSONObject("sys")
                    .getLong("sunset")*1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setWeatherIcon(int actualId, long sunrise, long sunset){
        int id = actualId/100;
//        String icon = "";
        Drawable img = getActivity().getDrawable(R.drawable.ic_sunny);
        if(actualId == 800) {
            long currentTime = new Date().getTime();
            if(currentTime >= sunrise && currentTime < sunrise){
//                icon = getActivity().getString(R.string.wheather_sunny);
                img = getActivity().getDrawable(R.drawable.ic_sunny);
            } else {
//                icon = getActivity().getString(R.string.wheather_clear_night);
                img = getActivity().getDrawable(R.drawable.ic_clear_night);
            }
        } else {
            switch (id) {
                case 2:
//                    icon = getActivity().getString(R.string.wheather_thunder);
                    img = getActivity().getDrawable(R.drawable.ic_thunder);
                    break;
                case 3:
//                    icon = getActivity().getString(R.string.wheather_drizzle);
                    img = getActivity().getDrawable(R.drawable.ic_drizzle);
                    break;
                case 7:
//                    icon = getActivity().getString(R.string.wheather_foggy);
                    img = getActivity().getDrawable(R.drawable.ic_foggy);
                    break;
                case 8:
//                    icon = getActivity().getString(R.string.wheather_cloudy);
                    img = getActivity().getDrawable(R.drawable.ic_cloudy);
                    break;
                case 6:
//                    icon = getActivity().getString(R.string.wheather_snowy);
                    img = getActivity().getDrawable(R.drawable.ic_snowy);
                    break;
                case 5:
//                    icon = getActivity().getString(R.string.wheather_rainy);
                    img = getActivity().getDrawable(R.drawable.ic_rainy);
                    break;
            }
        }
//        weatherField.setText(icon);
        weather_icon.setImageDrawable(img);
    }
    public void changeCity(String city) {
        updateWeatherData(city);
    }
}

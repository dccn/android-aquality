package ocow.com.customactionbar.weather;

import android.content.DialogInterface;
import android.location.Location;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import java.util.Locale;

import ocow.com.customactionbar.R;

public class WeatherActivity extends SwipeBackActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().add(R.id.weather_container,
                    new WeatherFragment()).commit();
        }
        setDragEdge(SwipeBackLayout.DragEdge.TOP);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.weather_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.change_city){
            showInputDialog();
        }

        return false;
    }

    private void showInputDialog(){
        AlertDialog.Builder builde = new AlertDialog.Builder(this);
        builde.setTitle("Change City");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builde.setView(input);
        builde.setPositiveButton("Go", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                changeCity("Dundalk,IE");
            }
        });
        Locale l = Locale.getDefault();

        changeCity(l.getDisplayName()+","+l.getCountry());
//        builde.show();
    }

    public void changeCity(String city){
        WeatherFragment wf = (WeatherFragment) getSupportFragmentManager()
                .findFragmentById(R.id.weather_container);
        wf.changeCity(city);

        new CityPreferences(this).setCity(city);
    }

}

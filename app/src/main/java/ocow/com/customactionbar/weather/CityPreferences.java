package ocow.com.customactionbar.weather;

import android.app.Activity;
import android.content.SharedPreferences;

/**
 * Created by dlark on 11/12/2017.
 */

public class CityPreferences {

    SharedPreferences prefs;

    public CityPreferences (Activity activity) {
        prefs = activity.getPreferences(Activity.MODE_PRIVATE);
    }

    String getCity() {
        return prefs.getString("city","Dundalk,IE");
    }

    void setCity(String city) {
        prefs.edit().putString("city", city).commit();
    }
}

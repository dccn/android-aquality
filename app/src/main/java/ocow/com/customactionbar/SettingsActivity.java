package ocow.com.customactionbar;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.Toast;

import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

public class SettingsActivity extends SwipeBackActivity {

    private SeekBar sk;
    Context context;
    int brightness;
    boolean success = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        sk = (SeekBar) findViewById(R.id.settings_seek);
        sk.setMax(255);
        sk.setProgress(getBrightness());

        getPermission();

        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(b && success) {
                    setBrightness(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(!success){
                    Toast.makeText(SettingsActivity.this,"Permission not granted", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // put everything else before this.
        setDragEdge(SwipeBackLayout.DragEdge.TOP);
    }

    private void setBrightness(int brightness) {
        if(brightness < 0) {
            brightness = 0;
        } else if(brightness > 255) {
            brightness = 255;
        }
        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
    }

    private int getBrightness() {
        int brightness = 100;
        try{
            ContentResolver contentResolver = getApplicationContext().getContentResolver();
            brightness = Settings.System.getInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return brightness;
    }

    private void getPermission() {
        boolean value;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            value = Settings.System.canWrite(getApplicationContext());
            if(value){
                success = true;
            } else {
                Intent in = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                in.setData(Uri.parse("package:"+getApplicationContext().getPackageName()));
                startActivityForResult(in, 1000);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1000) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                boolean value = Settings.System.canWrite(getApplicationContext());
                if(value) {
                    success = true;
                } else {
                    Toast.makeText(this,"Permission not granted", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}

package ocow.com.customactionbar;

/**
 * Created by dlarkin047 on 14/03/18.
 */

public class insectAbundanceList {
    int groupId;
    int amount;

    public insectAbundanceList(){}

    public insectAbundanceList(int groupId, int amount) {
        this.groupId = groupId;
        this.amount = amount;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}

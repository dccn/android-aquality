package ocow.com.customactionbar;

public class Insect {
    int id, group;
    String name, mini_desc, desc;

    public Insect() {
        
    }

    public Insect(int id, String name, String mini_desc, String desc, int group) {
        this.id = id;
        this.name = name;
        this.mini_desc = mini_desc;
        this.desc = desc;
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMini_desc() {
        return mini_desc;
    }

    public void setMini_desc(String mini_desc) {
        this.mini_desc = mini_desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

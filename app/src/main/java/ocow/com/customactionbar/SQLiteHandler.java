package ocow.com.customactionbar;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLInput;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;

public class SQLiteHandler extends SQLiteOpenHelper{
    // All Static Variables
    // Database version
    public static final int DATABASE_VERSION = 13;

    // Database Name
    public static final String DATABASE_NAME = "AQUAlity";

    // Insects Table Name
    public static final String TABLE_INSECTS = "insects";

    public static final String TABLE_IMG = "insects_img";

    // Insects Table Column Names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_MINI_DESC = "mini_desc";
    private static final String KEY_DESC = "desc";
    private static final String KEY_GROUP = "gp";

    private static final String KEY_IMG_ID = "img_id";
    private static final String KEY_IMG_NAME = "name";
    private static final String KEY_IMG = "img_data";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_INSECTS_TABLE = "CREATE TABLE " + TABLE_INSECTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, "
                + KEY_MINI_DESC + " TEXT, " + KEY_DESC + " TEXT, "
                + KEY_GROUP + " INTEGER )";
        db.execSQL(CREATE_INSECTS_TABLE);

        String CREATE_IMG_TABLE = "CREATE TABLE " + TABLE_IMG + "("
                + KEY_IMG_ID + " INTEGER, "
                + KEY_IMG_NAME + " TEXT, "
                + KEY_IMG + " TEXT )";
        db.execSQL(CREATE_IMG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop Older Table If Existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INSECTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMG);
        // Create Tables Again
        onCreate(db);
    }

    // Adding New Insect
    public void addInsect(Insect insect){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, insect.getName());
        values.put(KEY_MINI_DESC, insect.getMini_desc());
        values.put(KEY_DESC, insect.getDesc());
        values.put(KEY_GROUP, insect.getGroup());

        // Inserting Row
        db.insert(TABLE_INSECTS, null, values);
        db.close();
    }

    public void addImg(Drawable dbDrawable, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_IMG_NAME, name);
        Bitmap bitmap = ((BitmapDrawable)dbDrawable).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        values.put(KEY_IMG, stream.toByteArray());
        db.insert(TABLE_IMG, null, values);
        db.close();

        Log.e("TEST", "Image successfully added");
    }

//    public void addImg(byte[] bytes, String name) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//
//        values.put(KEY_IMG_NAME, name);
//        values.put(KEY_IMG, bytes);
//        db.insert(TABLE_IMG, null, values);
//        db.close();
//    }

    // Getting Single Insect
    public Insect getInsect(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_INSECTS, new String[] {
                        KEY_ID, KEY_NAME, KEY_MINI_DESC, KEY_DESC,
                        KEY_GROUP}, KEY_ID + "= ?",
                new String[] { String.valueOf(id) },
                null, null, null, null );
        if(cursor != null) {
            cursor.moveToFirst();
        }

        Insect insect = new Insect(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),
                cursor.getString(3),
                Integer.parseInt(cursor.getString(4)));

        return insect;
    }

    public ArrayList<InsectImg> getImg(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<InsectImg> insectImgArrayList = new ArrayList<>();

        Cursor cursor = db.query(TABLE_IMG, new String[] {
                        KEY_IMG_ID, KEY_IMG_NAME, KEY_IMG}, KEY_IMG_NAME +
                        " LIKE ? ", new String[] { name + "%"},
                null, null, null, null);

        InsectImg insectImg = new InsectImg();

        if(cursor.moveToFirst()) {
            do {
                insectImg.setImageId(cursor.getString(1));
                insectImg.setImageByteArray(cursor.getBlob(2));
                insectImgArrayList.add(insectImg);
            } while(cursor.moveToNext());
        }

        Log.e("TEST", "" + insectImgArrayList);

        cursor.close();
        db.close();
        return insectImgArrayList;
    }


    // Getting All Insects
    public List<Insect> getAllInsects() {
        List<Insect> insectList = new ArrayList<Insect>();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_INSECTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Looping Through All Rows And Adding To List
        if(cursor.moveToFirst()) {
            do {
                Insect insect = new Insect();
                insect.setId(Integer.parseInt(cursor.getString(0)));
                insect.setName(cursor.getString(1));
                insect.setMini_desc(cursor.getString(2));
                insect.setDesc(cursor.getString(3));
                insect.setGroup(Integer.parseInt(cursor.getString(4)));
                // Adding Insect To List
                insectList.add(insect);
            } while (cursor.moveToNext());
        }

        // Return Insect List
        return insectList;
    }

    // Getting Insects Count
    public int getInsectsCount() {
        String countQuery = "SELECT * FROM " + TABLE_INSECTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return cursor.getCount();
    }

    // Updating Single Insect
    public int updateInsect(Insect insect) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, insect.getName());
        values.put(KEY_MINI_DESC, insect.getMini_desc());
        values.put(KEY_DESC, insect.getDesc());
        values.put(KEY_GROUP, insect.getGroup());

        // Updating Rows
        return db.update(TABLE_INSECTS, values,
                KEY_ID + " = ?",
                new String[] { String.valueOf(insect.getId()) });
    }

    // Deleting Single Insect
    public void deleteInsect(Insect insect) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INSECTS, KEY_ID + " = ?",
                new String[] { String.valueOf(insect.getId()) });
        db.close();
    }

    // Check Insect Table Is Empty
    public boolean checkInsectTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        String countQuery = "SELECT count(*) FROM " + TABLE_INSECTS;

        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();

        int count = cursor.getInt(0);
        if(count > 0){
            return false;
        }
        else {
            return true;
        }
    }


    // Check Image Table Is Empty
    public boolean checkImageTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        String countQuery = "SELECT count(*) FROM " + TABLE_IMG;

        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();

        int count = cursor.getInt(0);
        if(count > 0) {
            return false;
        }
        else {
            return true;
        }
    }

}

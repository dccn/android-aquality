package ocow.com.customactionbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import ocow.com.customactionbar.accounts.ProfileActivity;
import ocow.com.customactionbar.weather.WeatherActivity;

/**
 * Created by dlark on 03/12/2017.
 */

public abstract class ToolbarConfig extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public abstract void toolBar();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent in = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(in);
            overridePendingTransition(R.anim.slide_up, 0);
            return true;
        }
        if(id == R.id.action_profile) {
            Intent in = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(in);
            overridePendingTransition(R.anim.slide_up,0);
            return true;
        }
        if(id == R.id.action_weather) {
            Intent in = new Intent(getApplicationContext(), WeatherActivity.class);
            startActivity(in);
            overridePendingTransition(R.anim.slide_up,0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

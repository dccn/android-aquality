package ocow.com.customactionbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.INotificationSideChannel;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.cast.framework.internal.featurehighlight.HelpTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ReviewForm extends ToolbarConfig {

    private String[] mainArray;
    private ArrayList<InsectClass> insectArray;
    public TinderActivity ta;
    public TextView stream_name, stream_desc, samplers, location, stream_width, wet_width, depth, comments, dept_mud;
    public TextView cobbles, gravel, sand, silt, siltation, animals, grassland, tillage, urban, forest, bog, clarity, velocity, riffle, rubbish, shading, suburban, natural, trees, grassland2;
    public TableLayout table;
    private DatabaseReference mDatabase;
    private Button submit;
    private FirebaseAuth auth;
    private FirebaseUser user;
    Calculator calc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_form);
        toolBar();
        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() == null) {
            Intent in = new Intent(ReviewForm.this, MainMenu.class);
            startActivity(in);
            finish();
        }

        calc = new Calculator();

        user = auth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("recordings");
        mDatabase.keepSynced(true);

        Bundle b = this.getIntent().getExtras();
        mainArray = b.getStringArray("mainArray");
        insectArray = (ArrayList<InsectClass>) b.getSerializable("insectArray");

        for (int i = 0; i <insectArray.size(); i++){
            Log.i("Review Page",""+insectArray.get(i).getName()+" " +insectArray.get(i).getAmount()+" "+insectArray.get(i).getGroup());
        }

        submit = (Button) findViewById(R.id.submit_btn);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitToDatabase();
                Toast.makeText(getApplicationContext(), "Thank you, your data has been submitted",Toast.LENGTH_SHORT).show();
                Intent in = new Intent(ReviewForm.this, MainMenu.class);
                startActivity(in);
                finish();
            }
        });

        findFields();
        loadData();
        loadSpinners();
        loadInsects();
    }

    public void toolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.aquality_logo);

        //        Toolbar back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        Returns to the previous Activity
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent in = new Intent(TinderActivity.this, GroupsActivity.class);
//                startActivity(in);
                finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            // Alert Dialog when exiting form.
            final AlertDialog.Builder builder = new AlertDialog.Builder(ReviewForm.this);
            builder.setTitle("Exiting Sample");
            builder.setMessage("Are you sure you want to exit? You will loose all data entered.");

            builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent in = new Intent(getApplicationContext(), MainMenu.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(in);
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //cancelled
                }
            });
            builder.create().show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void loadData(){
        stream_name.setText(mainArray[0]);
        stream_desc.setText(mainArray[1]);
        samplers.setText(mainArray[2]);
        location.setText(mainArray[3]+", "+mainArray[4]);
        stream_width.setText(mainArray[5]);
        wet_width.setText(mainArray[6]);
        depth.setText(mainArray[7]);
        dept_mud.setText(mainArray[13]);
        comments.setText(mainArray[15]);
    }

    public void loadSpinners(){
        String[] mainOpts = new String[]{
                "None", "Present", "Moderate", "Abundant"
        };

        String[] animalOpts = new String[] {
                "Full Access", "Semi-Controlled Drinking Point", "No Access"
        };

        String[] clarityOpts = new String[] {
                "Clear","Slightly Turbid or Coloured","Very Turbid or Coloured"
        };
        String[] velocityOpts = new String[] {
                "Fast","Moderate","Slow"
        };
        String[] riffleOpts = new String[] {
                "Riffle","Glide","Pool"
        };
//        final List<String> mainOptList = new ArrayList<>(Arrays.asList(mainOpts));
//        final List<String> animalOptList = new ArrayList<>(Arrays.asList(animalOpts));
//        final List<String> clarityOptList = new ArrayList<>(Arrays.asList(clarityOpts));
//        final List<String> velocityOptList = new ArrayList<>(Arrays.asList(velocityOpts));
//        final List<String> riffleOptList = new ArrayList<>(Arrays.asList(riffleOpts));
//
//        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item,mainOptList);
//        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
//
//        final ArrayAdapter<String> spinnerAnimalArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, animalOptList);
//        spinnerAnimalArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
//
//        final ArrayAdapter<String> spinnerClarityArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, clarityOptList);
//        spinnerClarityArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
//
//        final ArrayAdapter<String> spinnerVelocityArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, velocityOptList);
//        spinnerVelocityArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
//
//        final ArrayAdapter<String> spinnerRiffleArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, riffleOptList);
//        spinnerRiffleArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

//        cobbles.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[8], cobbles, mainOpts);
        cobbles.setText(mainArray[8]);

//        gravel.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[9], gravel, mainOpts);
        gravel.setText(mainArray[9]);

//        sand.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[10], sand, mainOpts);
        sand.setText(mainArray[10]);

//        silt.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[11], silt, mainOpts);
        silt.setText(mainArray[11]);

//        siltation.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[12], siltation, mainOpts);
        siltation.setText(mainArray[12]);

//        animals.setAdapter(spinnerAnimalArrayAdapter);
//        setSpinnerThree(mainArray[14], animals, animalOpts);
        animals.setText(mainArray[14]);

//        grassland.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[16],grassland, mainOpts);
        grassland.setText(mainArray[16]);

//        tillage.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[17],tillage, mainOpts);
        tillage.setText(mainArray[17]);

//        urban.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[18],urban, mainOpts);
        urban.setText(mainArray[18]);

//        forest.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[19],forest, mainOpts);
        forest.setText(mainArray[19]);

//        bog.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[20],bog, mainOpts);
        bog.setText(mainArray[20]);

//        clarity.setAdapter(spinnerClarityArrayAdapter);
//        setSpinnerThree(mainArray[21], clarity, clarityOpts);
        clarity.setText(mainArray[21]);

//        velocity.setAdapter(spinnerVelocityArrayAdapter);
//        setSpinnerThree(mainArray[22], velocity, velocityOpts);
        velocity.setText(mainArray[22]);

//        riffle.setAdapter(spinnerRiffleArrayAdapter);
//        setSpinnerThree(mainArray[23], riffle, riffleOpts);
        riffle.setText(mainArray[23]);

//        rubbish.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[24], rubbish, mainOpts);
        riffle.setText(mainArray[24]);

//        shading.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[25],shading,mainOpts);
        shading.setText(mainArray[25]);

//        suburban.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[26],suburban,mainOpts);
        suburban.setText(mainArray[26]);

//        natural.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[27],natural,mainOpts);
        natural.setText(mainArray[27]);

//        trees.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[28],trees,mainOpts);
        trees.setText(mainArray[28]);

//        grassland2.setAdapter(spinnerArrayAdapter);
//        setSpinner(mainArray[29],grassland2,mainOpts);
        grassland2.setText(mainArray[29]);
    }

    public void setSpinner(String array, Spinner spin, String[] opts){
        if(array.toLowerCase().equals(opts[0].toLowerCase())){
            spin.setSelection(0);
        } else if(array.toLowerCase().equals(opts[1].toLowerCase())){
            spin.setSelection(1);
        } else if(array.toLowerCase().equals(opts[2].toLowerCase())){
            spin.setSelection(2);
        } else if(array.toLowerCase().equals(opts[3].toLowerCase())){
            spin.setSelection(3);
        }
    }

    public void setSpinnerThree(String array, Spinner spin, String[] opts){
        if(array.toLowerCase().equals(opts[0].toLowerCase())){
            spin.setSelection(0);
        } else if(array.toLowerCase().equals(opts[1].toLowerCase())){
            spin.setSelection(1);
        } else if(array.toLowerCase().equals(opts[2].toLowerCase())){
            spin.setSelection(2);
        }
    }

    public void loadInsects() {
        table = (TableLayout) findViewById(R.id.table_review);
        table.setColumnStretchable(0,true);
        table.setColumnStretchable(1,true);
        for(int i = 0; i < insectArray.size(); i++){
//            TableRow tr = new TableRow(getApplicationContext());

            View tr = getLayoutInflater().inflate(R.layout.table_row, null);
            TextView tv = (TextView) tr.findViewById(R.id.insert_insect_name);
            TextView et = (TextView) tr.findViewById(R.id.insert_insect_amount);

            tv.setText(insectArray.get(i).getName());
            et.setText(insectArray.get(i).getAmount());

            table.addView(tr);
        }
    }

    public void submitToDatabase(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("Stream Name",stream_name.getText().toString());
        map.put("Stream Description",stream_desc.getText().toString());
        map.put("Samplers",samplers.getText().toString());
        map.put("Location",location.getText().toString());
        map.put("Stream Width",stream_width.getText().toString());
        map.put("Wet Width",wet_width.getText().toString());
        map.put("Average Depth",depth.getText().toString());
        map.put("Cobbles",cobbles.getText().toString());
        map.put("Gravel",gravel.getText().toString());
        map.put("Sand",sand.getText().toString());
        map.put("Silt or Mud",silt.getText().toString());
        map.put("Siltation",siltation.getText().toString());
        map.put("Mud Depth",dept_mud.getText().toString());
        map.put("Animals",animals.getText().toString());
        map.put("Comments",comments.getText().toString());
        map.put("Grassland",grassland.getText().toString());
        map.put("Tillage",tillage.getText().toString());
        map.put("Urban",urban.getText().toString());
        map.put("Forest",forest.getText().toString());
        map.put("Bog",bog.getText().toString());
        map.put("Clarity",clarity.getText().toString());
        map.put("Velocity",velocity.getText().toString());
        map.put("Riffle",riffle.getText().toString());
        map.put("Rubbish",rubbish.getText().toString());
        map.put("Shading",shading.getText().toString());
        map.put("Suburban",suburban.getText().toString());
        map.put("More Natural",natural.getText().toString());
        map.put("Trees",trees.getText().toString());
        map.put("Crops",grassland2.getText().toString());

        Map<String, String> insectMap = new HashMap<String, String>();
        for(int i=0;i<insectArray.size();i++){
            insectMap.put(insectArray.get(i).getName(),insectArray.get(i).getAmount());
        }
        insectMap.put("abundance", String.valueOf(calc.totalAbundance(insectArray)));

        Calendar t = Calendar.getInstance();
        DateFormat tf = new SimpleDateFormat("HH:mm:ss");
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String time = tf.format(t.getTime());
        String date = df.format(t.getTime());


        Map<String, String> details = new HashMap<String, String>();
        details.put("uid",user.getUid());
        details.put("date",date);
        details.put("time",time);

        Map<String, Map<String, String>> merged = new HashMap<String, Map<String, String>>();
        merged.put("environment",map);
        merged.put("insects",insectMap);
        merged.put("details",details);





        mDatabase.push().setValue(merged);
    }
    public void findFields(){
//        EditText's
        stream_name = (TextView) findViewById(R.id.stream_name);
        stream_desc = (TextView) findViewById(R.id.stream_desc);
        samplers = (TextView) findViewById(R.id.samplers);
        location = (TextView) findViewById(R.id.location);
        stream_width = (TextView) findViewById(R.id.stream_width);
        wet_width = (TextView) findViewById(R.id.wet_width_two);
        depth = (TextView) findViewById(R.id.avg_depth);
        comments = (TextView) findViewById(R.id.cattle_comments);
        dept_mud = (TextView) findViewById(R.id.dept_mud_spinner);
//        Spinner's
        cobbles = (TextView) findViewById(R.id.cobble_spinner);
        gravel = (TextView) findViewById(R.id.gravel_spinner);
        sand = (TextView) findViewById(R.id.sand_spinner);
        silt = (TextView) findViewById(R.id.mud_spinner);
        siltation = (TextView) findViewById(R.id.silitation_spinner);
        animals = (TextView) findViewById(R.id.animal_spinner);
        grassland = (TextView) findViewById(R.id.grass_spinner);
        tillage = (TextView) findViewById(R.id.tillage_spinner);
        urban = (TextView) findViewById(R.id.urban_spinner);
        forest = (TextView) findViewById(R.id.forest_spinner);
        bog = (TextView) findViewById(R.id.bog_spinner);
        clarity = (TextView) findViewById(R.id.clarity_spinner);
        velocity = (TextView) findViewById(R.id.velocity_spinner);
        riffle = (TextView) findViewById(R.id.riffle_spinner);
        rubbish = (TextView) findViewById(R.id.rubbish_spinner);
        shading = (TextView) findViewById(R.id.shading_spinner);
        suburban = (TextView) findViewById(R.id.suburnan_spinner);
        natural = (TextView) findViewById(R.id.natural_spinner);
        trees = (TextView) findViewById(R.id.trees_spinner);
        grassland2 = (TextView) findViewById(R.id.crop_spinner);
    }
}

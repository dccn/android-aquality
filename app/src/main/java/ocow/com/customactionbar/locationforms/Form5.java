package ocow.com.customactionbar.locationforms;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form5 extends Fragment {
    private EditText mud_spin;
    private TextView next_btn;
    private String input;
    private ImageView info_10;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.form5_frag,container,false);
        mud_spin = (EditText) v.findViewById(R.id.mud_spinner);
        next_btn = (TextView) v.findViewById(R.id.next_btn_f5);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input = mud_spin.getText().toString();
                if(input.equals("Please Select")){
                    mud_spin.setBackgroundResource(R.drawable.editview_shape_error);
                    mud_spin.setError("Please enter mud depth.");
                    return;
                }

                ((MainActivity)getActivity()).setForm5Array(input);
                ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
                ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
            }
        });

        setHints(v);
        return v;
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info_10 = (ImageView) v.findViewById(R.id.info_10);

        info_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Depth of Mud on Bottom", "How deep is the mud in the stream?");
            }
        });
    }
}

package ocow.com.customactionbar.locationforms;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import ocow.com.customactionbar.CustomListAdapter;
import ocow.com.customactionbar.ListViewItemsClass;
import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form6 extends Fragment {
    private ListView listView2;
    private CustomListAdapter mAdapter;
    private MainActivity ma;
    private int lv_temp_height;
    private TextView next_btn;
    private String input;
    private RelativeLayout opt1, opt2, opt3;
    private ImageView info_11;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.form6_frag,container,false);
        opt1 = (RelativeLayout) v.findViewById(R.id.opt_1);
        opt2 = (RelativeLayout) v.findViewById(R.id.opt_2);
        opt3 = (RelativeLayout) v.findViewById(R.id.opt_3);

        opt1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                input = "Full Access";
                opt1.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                opt2.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.transparent)));
                opt3.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.transparent)));
                nextFrag();

            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                input = "Semi-Controlled Drinking Point";
                opt2.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                opt1.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.transparent)));
                opt3.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.transparent)));
                nextFrag();
            }
        });
        opt3.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                input = "No Access";
                opt3.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                opt2.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.transparent)));
                opt1.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.transparent)));
                nextFrag();
            }
        });

        setHints(v);

        next_btn = (TextView) v.findViewById(R.id.next_btn_f6);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(input==null){
                    return;
                }
                nextFrag();
            }
        });
        return v;
    }

    private void nextFrag() {
        ((MainActivity)getActivity()).setForm6Array(input);
        ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
        ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info_11 = (ImageView) v.findViewById(R.id.info_11);

        info_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Do Cattle or other farm animals have access to the stream?", "Select one of the options that best describe the access for animals.");
            }
        });
    }
}

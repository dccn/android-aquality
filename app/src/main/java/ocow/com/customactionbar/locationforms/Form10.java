package ocow.com.customactionbar.locationforms;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form10 extends Fragment {

    private String input = "";
    private TextView next_btn;
    private ImageView info_15;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.form10_frag,container,false);
        next_btn = (TextView) v.findViewById(R.id.next_btn_f10);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(input == null || input.isEmpty()){
                    Toast.makeText(getActivity(),"Please choose an option.", Toast.LENGTH_SHORT).show();
                    return;
                }
                nextFrag();
            }
        });

        setHints(v);

        btns(v);
        return v;
    }

    private void btns(View v) {
        Button none = (Button) v.findViewById(R.id.none_btn);
        Button present = (Button) v.findViewById(R.id.present_btn);
        Button moderate = (Button) v.findViewById(R.id.moderate_btn);
        Button abundate = (Button) v.findViewById(R.id.abundant_btn);
        none.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = "none";
                checkInput();
            }
        });
        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = "present";
                checkInput();
            }
        });
        moderate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = "moderate";
                checkInput();
            }
        });
        abundate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = "abundant";
                checkInput();
            }
        });
    }

    private void nextFrag() {
        ((MainActivity)getActivity()).setForm10Array(input);
        ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
        ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
    }

    private void checkInput() {
        if(!input.isEmpty()) {
            nextFrag();
        }
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info_15 = (ImageView) v.findViewById(R.id.info_15);

        info_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Can you see any rubbish in the stream?", "Please choose one option.");
            }
        });
    }
}

package ocow.com.customactionbar.locationforms;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form4 extends Fragment {

    private TextView next_btn, prev_btn;
    private String input;
    private ImageView info_9;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.form4_frag,container,false);
        input = "";
        next_btn = (TextView) v.findViewById(R.id.next_btn_f4);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(input.isEmpty() || input == null){
                    Toast.makeText(getActivity(), "Please choose an option.", Toast.LENGTH_SHORT).show();
                    return;
                }
                nextFrag();
            }
        });

        setHints(v);

        btns(v);
        return v;
    }

    private void btns(View v) {
        Button none = (Button) v.findViewById(R.id.none_btn);
        Button present = (Button) v.findViewById(R.id.present_btn);
        Button moderate = (Button) v.findViewById(R.id.moderate_btn);
        Button abundate = (Button) v.findViewById(R.id.abundant_btn);
        none.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = "none";
                nextFrag();
            }
        });
        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = "present";
                nextFrag();
            }
        });
        moderate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = "moderate";
                nextFrag();
            }
        });
        abundate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = "abundant";
                nextFrag();
            }
        });
    }

    private void nextFrag() {
        ((MainActivity)getActivity()).setForm4Array(input);
        ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
        ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info_9 = (ImageView) v.findViewById(R.id.info_9);

        info_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Degree of Siltation", "Is silt released when you take a kcik sample? If so, how much?");
            }
        });
    }
}

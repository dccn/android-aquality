package ocow.com.customactionbar.locationforms;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import ocow.com.customactionbar.CustomListAdapter;
import ocow.com.customactionbar.ListViewItemsClass;
import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form3 extends Fragment {
    private ListView listView;
    public CustomListAdapter mAdapter;
    private View ll;
    private TextView next_btn;
    private String cobbles,gravel,sand, silt;
    private int position;
    private RelativeLayout cobble_btn, gravel_btn, sand_btn, silt_btn;
    private int opt_btn;
    private ImageView info_8;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.form3_frag,container,false);
        ll = (View) v.findViewById(R.id.popup_form3);
        next_btn = (TextView) v.findViewById(R.id.next_btn_f3);
        cobble_btn = (RelativeLayout) v.findViewById(R.id.opt_1);
        gravel_btn = (RelativeLayout) v.findViewById(R.id.opt_2);
        sand_btn = (RelativeLayout) v.findViewById(R.id.opt_3);
        silt_btn = (RelativeLayout)v.findViewById(R.id.opt_4);

        cobbles = "";
        gravel = "";
        sand = "";
        silt="";

        setHints(v);

//        tempItems();

//        listView.setAdapter(mAdapter);
        cobble_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                position = 0;
            }
        });
        gravel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                position = 1;
            }
        });
        sand_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                position = 2;
            }
        });
        silt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                position = 3;
            }
        });

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, final int pos, long l) {
//                ll.setVisibility(View.VISIBLE);
//                position = pos;
//
//            }
//        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cobbles.isEmpty() || cobbles==null){
                    Toast.makeText(getActivity(), "Please choose for \"Cobbles/Large Stones\"", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(gravel.isEmpty() ||gravel ==null){
                    Toast.makeText(getActivity(), "Please choose for \"Gravel\"", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(sand.isEmpty() || sand==null){
                    Toast.makeText(getActivity(), "Please choose for \"Sand\"", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(silt.isEmpty()||silt==null){
                    Toast.makeText(getActivity(), "Please choose for \"Silt/Mud\"", Toast.LENGTH_SHORT).show();
                    return;
                }

                ((MainActivity)getActivity()).setForm3Array(cobbles, gravel, sand, silt);
                ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
                ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
            }
        });

        btns(v);
        return v;
    }

    private void tempItems() {
        mAdapter = new CustomListAdapter(getActivity(),0, 0, 4);
        mAdapter.add(new ListViewItemsClass(R.drawable.cobbles_img,"Cobbles / Large Stones"));
        mAdapter.add(new ListViewItemsClass(R.drawable.gravel_img, "Gravel"));
        mAdapter.add(new ListViewItemsClass(R.drawable.sand_img, "Sand"));
        mAdapter.add(new ListViewItemsClass(R.drawable.mud_img, "Silt / Mud"));
    }

    private void btns(View v) {
        Button none = (Button) v.findViewById(R.id.none_btn);
        Button present = (Button) v.findViewById(R.id.present_btn);
        Button moderate = (Button) v.findViewById(R.id.moderate_btn);
        Button abundate = (Button) v.findViewById(R.id.abundant_btn);
        none.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_pos(position, "none");
                highlightBtn();
                ll.setVisibility(View.GONE);
            }
        });
        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_pos(position, "present");
                highlightBtn();
                ll.setVisibility(View.GONE);
            }
        });
        moderate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_pos(position, "moderate");
                highlightBtn();
                ll.setVisibility(View.GONE);
            }
        });
        abundate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_pos(position, "abundant");
                highlightBtn();
                ll.setVisibility(View.GONE);
            }
        });
    }

    private void highlightBtn() {
        if(position == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cobble_btn.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        }

        if(position == 1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                gravel_btn.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        }

        if(position == 2) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                sand_btn.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        }

        if(position == 3) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                silt_btn.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        }
    }

    private void item_pos(int pos, String btn_val){
        if(pos == 0) {
            cobbles = btn_val;
        } else if(pos == 1){
            gravel = btn_val;
        } else if(pos == 2) {
            sand = btn_val;
        } else {
            silt = btn_val;
        }
    }

    private void checkVal(){
        if (!cobbles.isEmpty() && !gravel.isEmpty() && !sand.isEmpty() && !silt.isEmpty()){
            ((MainActivity)getActivity()).setForm3Array(cobbles, gravel, sand, silt);
            ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
            ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
        }
    }
    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info_8 = (ImageView) v.findViewById(R.id.info_8);

        info_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Principle Type of Substratum Sampled", "Please select an option for each of the four types of substratum.");
            }
        });
    }
}

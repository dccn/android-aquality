package ocow.com.customactionbar.locationforms;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form7 extends Fragment {
    private EditText comments;
    private TextView next_btn;
    private String input;
    private ImageView info_12;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.form7_frag,container,false);
        comments = (EditText) v.findViewById(R.id.form7_et);
        next_btn = (TextView) v.findViewById(R.id.next_btn_f7);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input = comments.getText().toString();
                if(input==null || input.isEmpty()){
                    input = " ";
                }

                ((MainActivity)getActivity()).setForm7Array(input);

                ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
                ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
            }
        });

        setHints(v);
        return v;
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info_12 = (ImageView) v.findViewById(R.id.info_12);

        info_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Comments on animal access to the stream", "This is optional, but you can write a comment if there is something you feel warrants the comment?");
            }
        });
    }
}

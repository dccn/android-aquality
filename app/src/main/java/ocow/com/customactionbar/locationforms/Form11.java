package ocow.com.customactionbar.locationforms;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import ocow.com.customactionbar.CustomListAdapter;
import ocow.com.customactionbar.ListViewItemsClass;
import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form11 extends Fragment {
    private CustomListAdapter mAdapter;
    private View ll;
    private String shading = "", urban = "",natural = "", trees = "", crops = "";
    private TextView next_btn;
    private Button none, present, moderate, abundate;
    private RelativeLayout opt1, opt2, opt3, opt4, opt5;
    private ImageView info_16;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.form11_frag,container,false);

        ll = (View) v.findViewById(R.id.popup_form3);

        none = (Button) v.findViewById(R.id.none_btn);
        present = (Button) v.findViewById(R.id.present_btn);
        moderate = (Button) v.findViewById(R.id.moderate_btn);
        abundate = (Button) v.findViewById(R.id.abundant_btn);

        opt1 = (RelativeLayout) v.findViewById(R.id.opt_1);
        opt2 = (RelativeLayout) v.findViewById(R.id.opt_2);
        opt3 = (RelativeLayout) v.findViewById(R.id.opt_3);
        opt4 = (RelativeLayout) v.findViewById(R.id.opt_4);
        opt5 = (RelativeLayout) v.findViewById(R.id.opt_5);

        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                btns(v, 0);
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                btns(v, 1);
            }
        });
        opt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                btns(v, 2);
            }
        });
        opt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                btns(v, 3);
            }
        });
        opt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                btns(v, 4);
            }
        });

        setHints(v);

        next_btn = (TextView) v.findViewById(R.id.next_btn_f11);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(shading == null){
                    Toast.makeText(getActivity(),"Please choose an option for Degree of Shading", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(urban == null){
                    Toast.makeText(getActivity(),"Please choose an option for Urban/Suburban", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(natural == null){
                    Toast.makeText(getActivity(),"Please choose an option for More Natural", Toast.LENGTH_SHORT).show();
                    return; }
                if(trees == null){
                    Toast.makeText(getActivity(),"Please choose an option for Presence of Trees", Toast.LENGTH_SHORT).show();
                    return; }
                if(crops == null){
                    Toast.makeText(getActivity(),"Please choose an option for Grassland or Crop", Toast.LENGTH_SHORT).show();
                    return; }

                nextFrag();
            }
        });

        return v;
    }

    private void nextFrag() {
        ((MainActivity)getActivity()).setForm11Array(shading,urban,natural,trees,crops);
        ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
        ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
    }

    private void btns(View v, final int pos) {
        none.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_pos(pos, "none");
                ll.setVisibility(View.GONE);
            }
        });
        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_pos(pos, "present");
                ll.setVisibility(View.GONE);
            }
        });
        moderate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_pos(pos, "moderate");
                ll.setVisibility(View.GONE);
            }
        });
        abundate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_pos(pos, "abundant");
                ll.setVisibility(View.GONE);
            }
        });
    }

    private void item_pos(int pos, String btn_val){
        if(pos == 0) {
            shading = btn_val;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                opt1.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        } else if(pos == 1){
            urban = btn_val;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                opt2.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        } else if(pos == 2) {
            natural = btn_val;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                opt3.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        } else if (pos == 3) {
            trees = btn_val;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                opt4.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        } else {
            crops = btn_val;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                opt5.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
            }
            checkVal();
        }
    }

    private void checkVal() {
        if(!shading.isEmpty() && !urban.isEmpty() && !natural.isEmpty() && !trees.isEmpty() && !crops.isEmpty()) {
            nextFrag();
        }
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info_16 = (ImageView) v.findViewById(R.id.info_16);

        info_16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Stream Settling", "Please choose one option for each of the descriptions below. Select the best matches for your stream site.");
            }
        });
    }
}

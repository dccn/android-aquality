package ocow.com.customactionbar.locationforms;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form1 extends Fragment {

    public EditText streamName, desc, samplers, coordinates;
    private Button location;

    private int PICKER_REQUEST = 1;
    private String location_picker;
    private String name, descp, samp;
    private double latitude, longitude;
    private RelativeLayout pb;
    private TextView next_btn;
    private DatabaseReference db;
    private MainActivity ma;
    private ImageView info1,info2, info3,info4;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.form1_frag,container,false);
        db = FirebaseDatabase.getInstance().getReference();
        ma = new MainActivity();

        streamName = (EditText) v.findViewById(R.id.stream_name);
        desc = (EditText) v.findViewById(R.id.description_of_site);
        coordinates = (EditText) v.findViewById(R.id.coordinates);
        samplers = (EditText) v.findViewById(R.id.samplers);
        location = (Button) v.findViewById(R.id.location);
        pb = (RelativeLayout) v.findViewById(R.id.form1_progress);
        next_btn = (TextView) v.findViewById(R.id.next_btn_f1);

        setHints(v);

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = streamName.getText().toString();
                descp = desc.getText().toString();
                samp = samplers.getText().toString();
                String loc = coordinates.getText().toString();

                if(name.isEmpty()){
                    streamName.setBackgroundResource(R.drawable.editview_shape_error);
                    streamName.setError("Please Enter Stream Name.");
                    return;
                }
                if(descp.isEmpty()){
                    desc.setBackgroundResource(R.drawable.editview_shape_error);
                    desc.setError("Please Enter Stream Description.");
                    return;
                }
//                if(samp.isEmpty()){
//                    samplers.setBackgroundResource(R.drawable.editview_shape_error);
//                    samplers.setError("Please Enter Samplers Names.");
//                    return;
//                }
                if(loc.isEmpty()){
                    location.setError("Please Find Coordinates");
                    return;
                }

                ((MainActivity)getActivity()).setForm1Array(name,descp,samp,latitude,longitude);
                ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
                ((MainActivity)getActivity()).getViewPager().beginFakeDrag();

            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pb.setVisibility(View.VISIBLE);
                location.setError(null);
                location.clearFocus();
                ((MainActivity)getActivity()).hideSoftKeyBoard();
                googlePickerLocation();
            }
        });
        name = streamName.getText().toString();
        descp = desc.getText().toString();
        samp = samplers.getText().toString();
        return v;
    }

    public void googlePickerLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        Intent in;
        try {
            in = builder.build(getActivity());
            startActivityForResult(in,PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Log.i("ERROR 1", "repairableException");
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.i("ERROR 2", "Not Available Exception");
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pb.setVisibility(View.GONE);
        if(requestCode == PICKER_REQUEST) {
//            Log.i("TEST", "onActivityResult, 1st IF");
            if(resultCode == Activity.RESULT_OK) {
//                Log.i("TEST", "onActivityResult, 2nd IF");
                Place locat = PlacePicker.getPlace(data, getActivity());
                latitude = locat.getLatLng().latitude;
                longitude = locat.getLatLng().longitude;
                location_picker = String.format("%s", latitude + "," + longitude);
                coordinates.setVisibility(View.VISIBLE);
                coordinates.setText(location_picker);
                Toast.makeText(getContext(), location_picker, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info1 = (ImageView) v.findViewById(R.id.info_1);
        info2 = (ImageView) v.findViewById(R.id.info_2);
        info3 = (ImageView) v.findViewById(R.id.info_3);
        info4 = (ImageView) v.findViewById(R.id.info_4);

        info1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Stream Name", "This is the name of the stream you are wanting to sample.");
            }
        });

        info2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Description of Sampling Site", "Give a brief description of your surrounding area.");
            }
        });

        info3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Names of Samplers", "Enter the names of your group,If you are with one. If not just enter your name.");
            }
        });

        info4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Location", "Click on the grey location button and then select this location to return your current coordinates.");
            }
        });
    }
}

package ocow.com.customactionbar.locationforms;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form2 extends Fragment {
    private EditText streamWidth, wetWidth, avgDepth;
    private TextView next_btn;
    private String width, str_wet, depth;
    private ImageView info5,info6,info7;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.form2_frag,container,false);
        next_btn = (TextView) v.findViewById(R.id.next_btn_f2);

        streamWidth = (EditText) v.findViewById(R.id.stream_width);
        wetWidth = (EditText) v.findViewById(R.id.wet_width);
        avgDepth = (EditText) v.findViewById(R.id.avg_depth);

        setHints(v);

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                width = streamWidth.getText().toString();
                str_wet = wetWidth.getText().toString();
                depth = avgDepth.getText().toString();

                if(width.isEmpty()){
                    streamWidth.setBackgroundResource(R.drawable.editview_shape_error);
                    streamWidth.setError("Please Enter Stream Width.");
                    return;
                }
                if(str_wet.isEmpty()) {
                    wetWidth.setBackgroundResource(R.drawable.editview_shape_error);
                    wetWidth.setError("Please Enter Wet Width.");
                    return;
                }
                if(depth.isEmpty()) {
                    avgDepth.setBackgroundResource(R.drawable.editview_shape_error);
                    avgDepth.setError("Please Enter Average Depth");
                    return;
                }

                ((MainActivity)getActivity()).setForm2Array(width, str_wet, depth);
                ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
                ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
            }
        });
        return v;
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info5 = (ImageView) v.findViewById(R.id.info_5);
        info6 = (ImageView) v.findViewById(R.id.info_6);
        info7 = (ImageView) v.findViewById(R.id.info_7);

        info5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Width of Channel when Full", "Measure the the width of the stream.");
            }
        });

        info6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Current Wet Width", "This is the width of the channel at the water level.");
            }
        });

        info7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Average Depth at Sample Site (cm)", "Measure the depth of the stream a few times and then average them.");
            }
        });
    }

}

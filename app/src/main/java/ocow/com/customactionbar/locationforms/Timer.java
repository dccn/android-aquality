package ocow.com.customactionbar.locationforms;

import android.content.Intent;
import android.icu.util.TimeUnit;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ocow.com.customactionbar.GroupsActivity;
import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;
import ocow.com.customactionbar.TinderActivity;

import static android.R.attr.duration;

/**
 * Created by dlark on 09/11/2017.
 */

public class Timer extends Fragment {
    private Button start_btn, finish_btn;
    private ImageButton reset_btn;
    private CountDownTimer countDownTimer;
    private RelativeLayout layout1, layout2;
    private TextView timer_tv;
    private long duration;
    private MediaPlayer mp;
    private boolean alarm = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.timer_frag,container,false);

         mp = MediaPlayer.create(getActivity(), R.raw.alarm);

        layout1 = (RelativeLayout) v.findViewById(R.id.timer_content1);
        layout2 = (RelativeLayout) v.findViewById(R.id.timer_content2);
        timer_tv = (TextView) v.findViewById(R.id.timer_tv);

        start_btn = (Button) v.findViewById(R.id.start_btn);
        start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout1.setVisibility(View.GONE);
                layout2.setVisibility(View.VISIBLE);
                startTimer();
            }
        });

        finish_btn = (Button) v.findViewById(R.id.finished_btn);
        finish_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Finish button","????");
                alarm = true;
                mp.stop();

                Bundle b = new Bundle();
                b.putStringArray("mainArray", ((MainActivity)getActivity()).getFormArray());

                Intent in = new Intent(getActivity(), TinderActivity.class);
                in.putExtras(b);
                startActivity(in);
                getActivity().finish();
            }
        });
        reset_btn = (ImageButton) v.findViewById(R.id.reset_btn);
        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetTimer();
                mp.stop();
            }
        });
        return v;
    }

    private void startTimer() {
        duration = 180000; // 3 minutes
        countDownTimer = new CountDownTimer(duration, 1000) {
            @Override
            public void onTick(long l) {
                long secs = 1000;
                long mins = secs * 60;

                long elapsedMins = l / mins;
                l = l % mins;

                long elapsedSecs = l / secs;

                String time = String.format("%02d:%02d", elapsedMins, elapsedSecs);
                timer_tv.setText(time);

            }

            @Override
            public void onFinish() {
                timer_tv.setText("00:00");
                    mp.start();
                    finish_btn.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    private void resetTimer() {
        countDownTimer.cancel();
        startTimer();
    }
}

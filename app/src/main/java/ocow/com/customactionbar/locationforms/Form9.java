package ocow.com.customactionbar.locationforms;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import ocow.com.customactionbar.CustomListAdapter;
import ocow.com.customactionbar.ListViewItemsClass;
import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.R;

/**
 * Created by dlark on 09/11/2017.
 */

public class Form9 extends Fragment {
    private ListView listView3;
    private CustomListAdapter mAdapter;
    private MainActivity ma;
    private int lv_temp_height;
    private View ll;
    private Button bt1, bt2, bt3;
    private String btn1_ans = "", btn2_ans = "", btn3_ans = "";
    private TextView next_btn;
    private RelativeLayout opt1, opt2,opt3;
    private ImageView info_14;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.form9_frag,container,false);

        ma = new MainActivity();
        lv_temp_height = ma.vPagerHeight - 176;
//        listView3 = (ListView) v.findViewById(R.id.listView_form9);
        tempItems(lv_temp_height);

        bt1 = (Button) v.findViewById(R.id.present_btn);
        bt2 = (Button) v.findViewById(R.id.moderate_btn);
        bt3 = (Button) v.findViewById(R.id.abundant_btn);
        opt1 = (RelativeLayout) v.findViewById(R.id.opt_1);
        opt2 = (RelativeLayout) v.findViewById(R.id.opt_2);
        opt3 = (RelativeLayout) v.findViewById(R.id.opt_3);

        final View vv = v;
        ll = (View) v.findViewById(R.id.popup_form3);
        btns();

//        btns(v);


        next_btn = (TextView) v.findViewById(R.id.next_btn_f9);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn1_ans == null){
                    Toast.makeText(getActivity(),"Please choose option for Clarity", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(btn2_ans == null){
                    Toast.makeText(getActivity(),"Please choose option for Velocity", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(btn3_ans == null){
                    Toast.makeText(getActivity(),"Please choose option for Riffle / Glide / Pool", Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.i("form9",btn1_ans+" "+btn2_ans+" " + btn3_ans);

                nextFrag();
            }
        });
        setHints(v);

        return v;
    }

    private void nextFrag() {
        ((MainActivity)getActivity()).setForm9Array(btn1_ans,btn2_ans,btn3_ans);
        ((MainActivity)getActivity()).getViewPager().setCurrentItem(((MainActivity)getActivity()).getViewPager().getCurrentItem()+1);
        ((MainActivity)getActivity()).getViewPager().beginFakeDrag();
    }

    private void tempItems(int newHeight) {
        mAdapter = new CustomListAdapter(getActivity(),0, newHeight, 3);
        mAdapter.add(new ListViewItemsClass(R.drawable.clarity_img,"Clarity"));
        mAdapter.add(new ListViewItemsClass(R.drawable.velocity_img, "Velocity"));
        mAdapter.add(new ListViewItemsClass(R.drawable.riffle_img, "Riffle / Glide / Pool"));
    }

    private void btns() {
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String str1 = "Clear", str2 = "Slightly Turbid or Coloured", str3 = "Very Turbid or Coloured";
                bt1.setText(str1);
                bt2.setText(str2);
                bt3.setText(str3);

                bt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn1_ans = str1;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt1.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                bt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn1_ans = str2;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt1.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                bt3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn1_ans = str3;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt1.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                ll.setVisibility(View.VISIBLE);
                checkVal();
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String str1 = "Fast", str2 = "Moderate", str3 = "Slow";
                bt1.setText(str1);
                bt2.setText(str2);
                bt3.setText(str3);

                bt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn2_ans = str1;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt2.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                bt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn2_ans = str2;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt2.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                bt3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn2_ans = str3;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt2.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                ll.setVisibility(View.VISIBLE);
                checkVal();
            }
        });
        opt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String str1 = "Riffle", str2 = "Glide", str3 = "Pool";
                bt1.setText(str1);
                bt2.setText(str2);
                bt3.setText(str3);

                bt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn3_ans = str1;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt3.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                bt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn3_ans = str2;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt3.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                bt3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btn3_ans = str3;
                        ll.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            opt3.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.blue_trans)));
                        }
                    }
                });
                ll.setVisibility(View.VISIBLE);
                checkVal();
            }
        });
    }

    private void checkVal() {
        if(!btn1_ans.isEmpty() && !btn2_ans.isEmpty() && !btn3_ans.isEmpty()) {
            nextFrag();
        }
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints(View v) {
        info_14 = (ImageView) v.findViewById(R.id.info_14);

        info_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Water Clarity and Velocity", "Please choose one option for each of the descriptions below.");
            }
        });
    }
}

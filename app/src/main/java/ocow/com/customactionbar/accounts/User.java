package ocow.com.customactionbar.accounts;

/**
 * Created by dlark on 06/12/2017.
 */

public class User {
    public String email;
    public String firstname;
    public String lastname;

    public User(String username, String email, String firstname, String lastname) {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}

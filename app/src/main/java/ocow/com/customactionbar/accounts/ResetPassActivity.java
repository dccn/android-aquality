package ocow.com.customactionbar.accounts;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import ocow.com.customactionbar.R;

public class ResetPassActivity extends AppCompatActivity {

    private EditText input_email;
    private Button btnReset;
    private TextView btnPrev;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);
        input_email = (EditText) findViewById(R.id.forgot_email);
        btnReset = (Button) findViewById(R.id.forgot_btn);
        btnPrev = (TextView) findViewById(R.id.forgot_login);

        input_email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        auth = FirebaseAuth.getInstance();
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = input_email.getText().toString().trim();
                if(TextUtils.isEmpty(email)){
                    Toast.makeText(getApplication(), "Enter your registered email", Toast.LENGTH_SHORT).show();
                    return;
                }
                auth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()) {
                                    Toast.makeText(ResetPassActivity.this, "We have sent you an email with instructions", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ResetPassActivity.this, "Email failed to send", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }
}

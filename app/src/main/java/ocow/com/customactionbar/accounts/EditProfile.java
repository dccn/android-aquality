package ocow.com.customactionbar.accounts;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.EnumMap;

import ocow.com.customactionbar.AsteriskPasswordTransformationMethod;
import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.MainMenu;
import ocow.com.customactionbar.R;
import ocow.com.customactionbar.TinderActivity;
import ocow.com.customactionbar.ToolbarConfig;

public class EditProfile extends ToolbarConfig implements View.OnClickListener {

    private Button new_img, new_email, upload_img, new_pass;
    private StorageReference mStorageRef, imgRef;
    private FirebaseUser user;
    private FirebaseAuth auth;
    private Uri selectedImg;
    private UploadTask uploadTask;
    private ProgressDialog pd;
    private ImageView img;
    private EditText new_email_et, new_pass_et;
    private boolean upload_int = false;
    private boolean pressPassUpdate = false;

    private int GALLERY_INTENT = 100;
    //camera == 1 gallery == 100

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        toolBar();

        mStorageRef = FirebaseStorage.getInstance().getReference();
        new_img = (Button) findViewById(R.id.newpic_btn);
        new_email = (Button) findViewById(R.id.newemail_btn);
        upload_img = (Button) findViewById(R.id.upload_img);
        img = (ImageView) findViewById(R.id.preview_img);
        new_email_et = (EditText) findViewById(R.id.new_email_et);
        new_pass = (Button) findViewById(R.id.newpass_btn);
        new_pass_et = (EditText) findViewById(R.id.new_pass_et);

        new_img.setOnClickListener(this);

        upload_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImage(upload_int);
            }
        });
        new_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new_email.setVisibility(View.GONE);
//                new_email_et.setVisibility(View.VISIBLE);
            }
        });
        new_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new_pass.setVisibility(View.GONE);
                auth.sendPasswordResetEmail(user.getEmail())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()) {
                                    notification();
                                    Toast.makeText(getApplicationContext(), "We have sent you an email with instructions", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Email failed to send", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

    }

    public void notification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("Email Verification");
        builder.setContentText("An email has been sent to your registered email address for verification.");
        Intent intent = new Intent(this, MainMenu.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent pi = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pi);
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(0, builder.build());
    }

    public void onClick(View v) {
        registerForContextMenu(v);
        this.openContextMenu(v);
        unregisterForContextMenu(v);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.camera_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.context_camera:
//                lauchCamera();
                return true;
            case R.id.context_gallery:
                launchGallery();
                return true;
        }
        return false;
    }

    public void lauchCamera() {
        Intent i = new Intent(Intent.ACTION_CAMERA_BUTTON);
        i.setType("image/*");
        startActivityForResult(i, 1);
        upload_int = true;
    }

    public void launchGallery() {
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i, 100);
        upload_int = true;
    }

    public void toolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.aquality_logo);

        //Toolbar back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Returns to the previous Activity (MainMenu)
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
                if (resultCode == RESULT_OK && data != null && data.getData() != null){
                    selectedImg = data.getData();
                    new_img.setText("Pick Another");
                    img.setVisibility(View.VISIBLE);
                    Picasso.with(EditProfile.this).load(selectedImg).into(img);
                } else {
                    new_img.setError("Error Finding Image");
                }
    }

    public void uploadImage(boolean bool) {
        if(bool) {
            uploadNewImage();
        } else {
//            userUpdate();
//            resetUserPassword();
            if(user.getEmail() != null) {
                if(user.isEmailVerified()){
                    FirebaseAuth.getInstance().sendPasswordResetEmail(user.getEmail())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Log.d("Reset Password","Completed");
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("Reset Password", "Failed");
                        }
                    }).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("Reset Password", "Successful");
                        }
                    });
                } else{
                    Log.d("Email", "Null");
                }
        }
    }
    }

    //Something is wrong with authenticating current email....

    public void userUpdate() {
        String email = new_email_et.getText().toString();
        String pass = new_pass_et.getText().toString();
        Log.d("email file",""+email);
        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass)){
            //update email in database.
            reAuthenticateUser(true, true, pass, email);
        } else if(TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass)){
            reAuthenticateUser(true, false, pass, email);
        } else if (!TextUtils.isEmpty(email) && TextUtils.isEmpty(pass)){
            reAuthenticateUser(false, true, pass, email);
        }
    }

    public void uploadNewImage() {

//        userUpdate();
        resetUserPassword();

        upload_int = false;
        imgRef = mStorageRef.child("users/"+user.getUid()+".jpg");
        pd = new ProgressDialog(this);
        pd.setMax(100);
        pd.setMessage("Uploading...");
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.show();
        pd.setCancelable(false);

        uploadTask = imgRef.putFile(selectedImg);
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                pd.incrementProgressBy((int) progress);
            }
        });
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                upload_img.setError("Error uploading image. Try again.");
                pd.dismiss();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUri = taskSnapshot.getDownloadUrl();
                pd.dismiss();
            }
        });
    }

    public void resetUserPassword() {
//        user.isEmailVerified();
        if(!pressPassUpdate) {
            if(user.getEmail() != null) {
                if(user.isEmailVerified()){
                    auth.sendPasswordResetEmail(user.getEmail())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Log.d("Reset Password","Completed");
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("Reset Password", "Failed");
                        }
                    }).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("Reset Password", "Successful");
                        }
                    });
                } else{
                    Log.d("Email", "Null");
                }
            } else {
                user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d("Verify Email", "email sent");
                    }
                });
            }
            pressPassUpdate = true;
        } else {
            pressPassUpdate = false;
        }

    }

    public void reAuthenticateUser(final boolean pass, final boolean email, final String updatePassword, final String updateEmail) {
        LinearLayout layout = new LinearLayout(EditProfile.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText et_email = new EditText(this);
        et_email.setHint("Email");
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10,10,10,10);
        et_email.setLayoutParams(lp);
        layout.addView(et_email);

        final EditText et_pass = new EditText(this);
        et_pass.setHint("Password");
        et_pass.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp2.setMargins(10,10,10,10);
        et_pass.setLayoutParams(lp2);
        layout.addView(et_pass);

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(EditProfile.this);
        builder.setView(layout);
        builder.setTitle("Re-Authenticate");
        builder.setMessage("Please enter your current login details.");

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("okay buttons", "test");
                String tEmail = et_email.getText().toString();
                String tPass = et_pass.getText().toString();
                if(TextUtils.isEmpty(tEmail) || TextUtils.isEmpty(tPass)){
                    return;
                } else {
                    authenticate(pass, email, tPass,tEmail, updatePassword, updateEmail);
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("cancel Button", "Test");
            }
        });
        builder.create().show();
    }

    public void authenticate(final boolean pass, final boolean email, final String tEmail, final String tPass, final String updatePassword, final String updateEmail) {
        AuthCredential credential = EmailAuthProvider.getCredential(tEmail,tPass);
        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d("Authentication","Successful");
                    if(pass) {
                        //Password
                        updatePasswordForUser(pass,email,tEmail,tPass,updatePassword,updateEmail);
                    }
                    if(email){
                        //email
                        updateEmailForUser(updateEmail);
                    }
                }
            }
        });
    }

    public void updateEmailForUser(String email) {
        user.updateEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d("Update email","success");
                                user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Toast.makeText(EditProfile.this, "Email has been updated", Toast.LENGTH_LONG).show();
                                            Log.d("Edit Profile", "Verification Email sent");
                                        }
                                    }
                                });
                        }
                    }
                });
    }

    public void updatePasswordForUser(final boolean pass, final boolean email, final String tEmail, final String tPass, final String updatePassword, final String updateEmail) {
        user.updatePassword(updatePassword)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d("Update Password","success");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                AuthCredential credential = EmailAuthProvider.getCredential(tEmail,tPass);
                user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d("Authentication Password","Successful");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Authentication Password","Failed");
                    }
                });
            }
        });
    }
}
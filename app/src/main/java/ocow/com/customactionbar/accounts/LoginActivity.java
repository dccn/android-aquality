package ocow.com.customactionbar.accounts;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.uxcam.OnVerificationListener;
import com.uxcam.UXCam;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import ocow.com.customactionbar.AsteriskPasswordTransformationMethod;
import ocow.com.customactionbar.MainActivity;
import ocow.com.customactionbar.MainMenu;
import ocow.com.customactionbar.R;


public class LoginActivity extends AppCompatActivity {

    private EditText input_email, input_pass;
    private FirebaseAuth auth;
    private ProgressBar progressBar;
    private Button btnLogin;
    private TextView btnReset;
    private FirebaseAnalytics firebaseAnalytics;
    private MixpanelAPI mixpanelAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() != null) {
            Intent in = new Intent(LoginActivity.this, MainMenu.class);
            startActivity(in);
            finish();
        }
        setContentView(R.layout.activity_login);

        mixpanelAPI = MixpanelAPI.getInstance(this, "b4c3f5a97ec1f88c504719c9732f6061");
        UXCam.startWithKey("c8802510444c88c");
        UXCam.addVerificationListener(new OnVerificationListener() {
            @Override
            public void onVerificationSuccess() {
                JSONObject eventProps = new JSONObject();
                try{
                    eventProps.put("UXCAM:Session Recording link", UXCam.urlForCurrentSession());
                } catch (JSONException e){
                    e.printStackTrace();
                }
                mixpanelAPI.track("UXCam Session URL", eventProps);
                mixpanelAPI.getPeople().set("USCam User URL", UXCam.urlForCurrentUser());
            }

            @Override
            public void onVerificationFailed(String s) {

            }
        });


        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        input_email = (EditText) findViewById(R.id.login_email);
        input_pass = (EditText) findViewById(R.id.login_pass);
        btnLogin = (Button) findViewById(R.id.login_btn);
        btnReset = (TextView) findViewById(R.id.login_reset_pass);

        input_email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        input_pass.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(LoginActivity.this, ResetPassActivity.class);
                startActivity(in);
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = input_email.getText().toString();
                final String pass = input_pass.getText().toString();
                if(TextUtils.isEmpty(email)) {
                    Log.d("Its empty","email");
                    input_email.setBackgroundResource(R.drawable.editview_shape_error);
                    input_email.setError("Please enter address");
//                    Toast.makeText(LoginActivity.this, "Please Enter Email Address", Toast.LENGTH_LONG).show();
                    return;
                }
                if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    input_email.setBackgroundResource(R.drawable.editview_shape_error);
                    input_email.setError("This is not an email");
//                    Toast.makeText(LoginActivity.this, "Please make sure you are entering an email address", Toast.LENGTH_LONG).show();
                }
                if(TextUtils.isEmpty(pass)) {
                    Log.d("Its empty","pass");
                    input_pass.setBackgroundResource(R.drawable.editview_shape_error);
                    input_pass.setError("Please enter Password");
                    return;
                }
                login(email, pass);

            }
        });
    }

    public void login(String email, final String  pass){
        auth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()){
                            if(pass.length() < 6){
                                input_pass.setError("Password too short");
                            }
                            btnLogin.setError("Incorrect Email or Password");
                        } else {
                            checkEmailVerification();
                            mixpanelAPI.identify(auth.getUid());
                            mixpanelAPI.getPeople().identify(auth.getUid());
                        }
                    }
                });
    }

    public void checkEmailVerification(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//        if(user.isEmailVerified()){
            Intent intent = new Intent(LoginActivity.this, MainMenu.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();
            finish();

//        } else {
//            input_email.setError("Email has not been verified");
//            FirebaseAuth.getInstance().signOut();
//        }
    }

    @Override
    protected void onDestroy() {
        mixpanelAPI.flush();
        super.onDestroy();
    }
}


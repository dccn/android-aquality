package ocow.com.customactionbar.accounts;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import ocow.com.customactionbar.MainMenu;
import ocow.com.customactionbar.R;

public class ProfileActivity extends SwipeBackActivity {

    private TextView firstname, lastname, email, logout;
    private FirebaseUser user;
    private FirebaseAuth auth;
    private String sEmail, sFirstname, sLastname;
    private DatabaseReference database;
    private DatabaseReference myRef1, myRef2, myRef3;
    private StorageReference mStorageRef;
    private CircleImageView pro_img;
    private ImageButton editPro;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference("users");
        mStorageRef = FirebaseStorage.getInstance().getReference();
        database.keepSynced(true);

        if(auth.getCurrentUser() == null) {
            Intent in = new Intent(ProfileActivity.this, MainMenu.class);
            startActivity(in);
            finish();
        }

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        myRef1 = database.child(user.getUid()).child("email");
        myRef2 = database.child(user.getUid()).child("firstname");
        myRef3 = database.child(user.getUid()).child("lastname");

        String user_email = auth.getCurrentUser().getEmail(); //Do this better! Need a database sorted out
        String user_uid = user.getUid();


        email = (TextView) findViewById(R.id.profile_email);
        lastname = (TextView) findViewById(R.id.profile_lastname);
        firstname = (TextView) findViewById(R.id.profile_firstname);
        logout = (TextView) findViewById(R.id.profile_logout);
        pro_img = (CircleImageView) findViewById(R.id.profile_img);
        editPro = (ImageButton) findViewById(R.id.profile_edit_btn);

        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sEmail = dataSnapshot.getValue(String.class);
                email.setText(sEmail);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        myRef2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sFirstname = dataSnapshot.getValue(String.class);
                firstname.setText(sFirstname);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        myRef3.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sLastname = dataSnapshot.getValue(String.class);
                lastname.setText(sLastname);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mStorageRef.child("users/"+user_uid+".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                if(uri != null) {
                    Picasso.with(ProfileActivity.this).load(uri).fit().centerCrop().into(pro_img);
                }
//                pro_img.setImageURI(uri);
                Log.d("URIpic",""+uri);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Need to handle Errors
                Log.d("ERROR pro pic",":(");
            }
        });



        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                Intent in = new Intent(ProfileActivity.this, LoginActivity.class);
                startActivity(in);
                finish();
            }
        });

        editPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(ProfileActivity.this, EditProfile.class);
                startActivity(in);
                finish();
            }
        });

        setDragEdge(SwipeBackLayout.DragEdge.TOP);
    }
}

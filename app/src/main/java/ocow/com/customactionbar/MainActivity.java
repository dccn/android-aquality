package ocow.com.customactionbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import ocow.com.customactionbar.customwidgets.CustomViewPager;
import ocow.com.customactionbar.locationforms.Form1;
import ocow.com.customactionbar.locationforms.Form10;
import ocow.com.customactionbar.locationforms.Form11;
import ocow.com.customactionbar.locationforms.Form2;
import ocow.com.customactionbar.locationforms.Form3;
import ocow.com.customactionbar.locationforms.Form4;
import ocow.com.customactionbar.locationforms.Form5;
import ocow.com.customactionbar.locationforms.Form6;
import ocow.com.customactionbar.locationforms.Form7;
import ocow.com.customactionbar.locationforms.Form8;
import ocow.com.customactionbar.locationforms.Form9;

public class MainActivity extends ToolbarConfig{

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private Toolbar toolbar;
    private SwipeRefreshLayout vp;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public CustomViewPager mViewPager;
    public int vPagerHeight;
    public String[] arr;
    public SharedPreferences sharedPreferences;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolBar();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        vp = (SwipeRefreshLayout) findViewById(R.id.fragment_view);
        vp.setEnabled(false);

        arr = new String[30];

        // Set up the ViewPager with the sections adapter.
        mViewPager = (CustomViewPager) findViewById(R.id.container);
        mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setPagingEnabled(false);
        CircleIndicator ci = (CircleIndicator) findViewById(R.id.indicator);
        ci.setViewPager(mViewPager);
        arr[0] = "Test";

        sharedPreferences = getSharedPreferences("enviroment",0);

        Log.d("DataPassed",""+arr);
    }

    @Override
    public void toolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.aquality_logo);

        //Toolbar back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Returns to the previous Activity
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, MainMenu.class);
                startActivity(in);
                finish();
            }
        });
    }

    public void prev_fragment (View view) {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem()-1,true);
        mViewPager.beginFakeDrag();
    }

    public void go_home (View view) {
        Intent intent = new Intent(getApplicationContext(), MainMenu.class);
        startActivity(intent);
        finish();
    }

//    public void makeItemGone(ListView lv) {
//        for(int i = 0; i < lv.getChildCount(); i++ ) {
////            clickItems(i, lv).findViewById(R.id.buttons).setVisibility(View.GONE);
//            lv.findViewById(R.id.buttons).setVisibility(View.GONE);
//        }
//    }
//
//    public void makeItemVisible(View v, ListView lv, int pos){
////        lv.findViewById(R.id.buttons).setVisibility(View.GONE);
//        v.findViewById(R.id.buttons).setVisibility(View.VISIBLE);
//        final int firstListItem = lv.getFirstVisiblePosition();
//        final int childIndex = pos - firstListItem;
//        lv.setSelection(childIndex);
//        Log.d("CHILD", ""+childIndex);
//    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            // Alert Dialog when exiting form.
            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Exiting Sample");
            builder.setMessage("Are you sure you want to exit? You will loose all data entered.");

            builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent in = new Intent(getApplicationContext(), MainMenu.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(in);
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //cancelled
                }
            });
            builder.create().show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public View clickItems(int pos, ListView lv) {
        final int firstListItem = lv.getFirstVisiblePosition();
        final int lastListItem = firstListItem + lv.getChildCount() - 1;

        if(pos < firstListItem || pos > lastListItem) {
            return lv.getAdapter().getView(pos, null , lv);
        } else {
            final int childIndex = pos - firstListItem;
            return lv.getChildAt(childIndex);
        }
    }

    public void buttonsFour(View view, Button btn1, Button btn2, Button btn3, Button btn4) {
        btn1 = (Button) view.findViewById(R.id.none_btn);
        btn2 = (Button) view.findViewById(R.id.present_btn);
        btn3 = (Button) view.findViewById(R.id.moderate_btn);
        btn4 = (Button) view.findViewById(R.id.abundant_btn);

        final View v = view;

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Add to an array list.
                v.findViewById(R.id.buttons).setVisibility(View.GONE);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Add to an array list.
                v.findViewById(R.id.buttons).setVisibility(View.GONE);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Add to an array list.
                v.findViewById(R.id.buttons).setVisibility(View.GONE);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Add to an array list.
                v.findViewById(R.id.buttons).setVisibility(View.GONE);
            }
        });

    }

    public ViewPager getViewPager() {
        if(null == mViewPager) {
            mViewPager = (CustomViewPager) findViewById(R.id.container);
        }
        return mViewPager;
    }

    public String[] getFormArray() {
        return arr.clone();
    }

    public void setForm1Array(String name, String desc, String samplers, double lati, double longi) {
        arr[0] = name;
        arr[1] = desc;
        arr[2] = samplers;
        arr[3] = String.format(String.valueOf(lati));
        arr[4] = String.format(String.valueOf(longi));
    }

    public void setForm2Array(String str_width, String wetWidth, String depth) {
        arr[5] = str_width;
        arr[6] = wetWidth;
        arr[7] = depth;
    }

    public void setForm3Array(String cobbles, String gravel, String sand, String silt){
        arr[8] = cobbles;
        arr[9] = gravel;
        arr[10] = sand;
        arr[11] = silt;
    }

    public void setForm4Array(String str) {
        arr[12] = str;
    }

    public void setForm5Array(String mud) {
        arr[13] = mud;
    }

    public void setForm6Array(String animal){
        arr[14] = animal;
    }

    public void setForm7Array(String comments){
        arr[15] = comments;
    }

    public void setForm8Array(String grassland, String crops, String urban, String forest, String bog){
        arr[16] = grassland;
        arr[17] = crops;
        arr[18] = urban;
        arr[19] = forest;
        arr[20] = bog;
    }

    public void setForm9Array(String clarity, String velocity, String riffle){
        arr[21] = clarity;
        arr[22] = velocity;
        arr[23] = riffle;
    }

    public void setForm10Array(String rubbish) {
        arr[24] = rubbish;
    }

    public void setForm11Array(String shading, String urban, String natural, String trees, String crops){
        arr[25] = shading;
        arr[26] = urban;
        arr[27] = natural;
        arr[28] = trees;
        arr[29] = crops;
    }

    public void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if(imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
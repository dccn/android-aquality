package ocow.com.customactionbar;

/**
 * Created by dlark on 02/12/2017.
 */

public class ArrVals {
    private int image;

    public ArrVals(int img) {
        this.image = img;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}

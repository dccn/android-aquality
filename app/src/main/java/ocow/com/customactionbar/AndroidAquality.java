package ocow.com.customactionbar;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by dlark on 02/12/2017.
 */

public class AndroidAquality extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}

package ocow.com.customactionbar;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextClock;
import android.widget.TextView;

import java.util.ArrayList;

import ocow.com.customactionbar.ArrVals;
import ocow.com.customactionbar.R;

import static android.R.id.list;
import static java.security.AccessController.getContext;
import static ocow.com.customactionbar.R.id.container;

/**
 * Created by dlark on 02/12/2017.
 */

public class TinderGalleryAdapter extends PagerAdapter{

    Context cxt;
    LayoutInflater inflater;
    int count = 0;

    ArrayList<ArrVals> list = new ArrayList<>();
    ArrVals[] arr = new ArrVals[count];

    public TinderGalleryAdapter(Context context, ArrayList<ArrVals> items) {
        this.cxt = context;
        this.list = items;
        this.count = items.size();
        inflater = (LayoutInflater) cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.tinder_onclick_item, container, false);

        ImageView iv = (ImageView) v.findViewById(R.id.tinder_onclick_imageView);

        iv.setImageResource(list.get(position).getImage());

        container.addView(v);
        return v;
    }


}

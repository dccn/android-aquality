package ocow.com.customactionbar;

import java.io.Serializable;

/**
 * Created by dlarkin047 on 11/02/18.
 */

public class InsectClass implements Serializable {
    String name;
    String amount;
    int group;

    public InsectClass(String name, String amount, int group) {
        this.name = name;
        this.amount = amount;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }
}

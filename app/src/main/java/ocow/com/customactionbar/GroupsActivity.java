package ocow.com.customactionbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GroupsActivity extends ToolbarConfig {
    RelativeLayout gp1_rl, gp2_rl, gp3_rl, gp4_rl, gp5_rl, notsure_rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        toolBar();
        btns();

        Bundle b = this.getIntent().getExtras();
        String[] temp = b.getStringArray("mainArray");

        for(int i =0; i < temp.length; i++){
            Log.i("groups",""+temp[i]);
        }
    }

    @Override
    public void toolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.aquality_logo);
    }

    private void btns() {
        gp1_rl = (RelativeLayout) findViewById(R.id.group1_btn);
        gp2_rl = (RelativeLayout) findViewById(R.id.group2_btn);
        gp3_rl = (RelativeLayout) findViewById(R.id.group3_btn);
        gp4_rl = (RelativeLayout) findViewById(R.id.group4_btn);
        gp5_rl = (RelativeLayout) findViewById(R.id.group5_btn);
        notsure_rl = (RelativeLayout) findViewById(R.id.notsure_btn);

        gp1_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), TinderActivity.class);
                in.putExtra("group",1);
                startActivity(in);

            }
        });
        gp2_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), TinderActivity.class);
                in.putExtra("group",2);
                startActivity(in);

            }
        });
        gp3_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), TinderActivity.class);
                in.putExtra("group",3);
                startActivity(in);

            }
        });
        gp4_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), TinderActivity.class);
                in.putExtra("group",4);
                startActivity(in);

            }
        });
        gp5_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), TinderActivity.class);
                in.putExtra("group",5);
                startActivity(in);

            }
        });
        notsure_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Create a new Activity for this - new form.
            }
        });
    }
}

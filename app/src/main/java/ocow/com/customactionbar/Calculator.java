package ocow.com.customactionbar;

import android.util.Log;

import java.util.ArrayList;

public class Calculator {

    public int totalAbundance(ArrayList<InsectClass> insectArr) {
        int g1_total = 0, g2_total = 0, g3_total = 0, g4_total = 0, g5_total = 0;
        int g1_types = 0, g2_types = 0, g3_types = 0, g4_types = 0, g5_types = 0;
        ArrayList<insectAbundanceList> inArr = new ArrayList<>();

        for(int i = 0; i< insectArr.size(); i++){
            int group = insectArr.get(i).getGroup();
            int amt = relativeAbundance(Integer.parseInt(insectArr.get(i).getAmount()));
            inArr.add(new insectAbundanceList(group, amt));
            Log.i("Cal", "Rel: "+ inArr.get(i).getGroupId()+ " " + inArr.get(i).getAmount());
        }

        for(int i = 0; i < inArr.size(); i++) {
            if(inArr.get(i).getGroupId() == 1) {
                g1_total += inArr.get(i).getAmount();
                g1_types += 1;
            } else if(inArr.get(i).getGroupId() == 2){
                g2_total += inArr.get(i).getAmount();
                g2_types++;
            } else if(inArr.get(i).getGroupId() == 3){
                g3_total += inArr.get(i).getAmount();
                g3_types++;
            } else if(inArr.get(i).getGroupId() == 4) {
                g4_total += inArr.get(i).getAmount();
                g4_types++;
            } else {
                g5_total += inArr.get(i).getAmount();
                g5_types++;
            }
        }

        int g1_score = group1Score(g1_types, g1_total);
        int g2_score = group2Score(g2_types, g2_total);
        int g3_score = group3Score(g3_types, g3_total);
        int g4_score = group4Score(g4_types, g4_total);
        int g5_score = group5Score(g5_types);

        Log.i("Cal", "Score: "+g1_score + g2_score + g3_score + g4_score + g5_score);


        return g1_score + g2_score + g3_score + g4_score + g5_score;
    }

    public int relativeAbundance(int numOfInsects) {
        if(numOfInsects == 0) {
            return 0;
        }
        else if(numOfInsects >= 1 && numOfInsects <= 5) {
            return 1;
        }
        else if(numOfInsects >= 6 && numOfInsects <= 20) {
            return 2;
        }
        else if(numOfInsects >= 21 && numOfInsects <= 50) {
            return 3;
        }
        else if(numOfInsects >= 51 && numOfInsects <= 100) {
            return 4;
        }
        else {
            return 5;
        }
    }

    public int group1Score(int numOfTypes, int relativeAbundance) {
        if(numOfTypes == 0) {
            return 0;
        }
        else if(numOfTypes == 1) {
            if(relativeAbundance >= 1 && relativeAbundance <= 2) {
                return 4;
            }
            else if(relativeAbundance >= 3){
                return 6;
            }
        }
        else {
            if(relativeAbundance == 2) {
                return 4;
            }
            else if(relativeAbundance >= 3) {
                return 8;
            }
        }
        return 0;
    }

    public int group2Score(int numOfTypes, int relativeAbundance) {
        if(numOfTypes == 0) {
            return 0;
        }
        else if(numOfTypes == 1) {
            if(relativeAbundance >= 1 && relativeAbundance <= 2) {
                return 4;
            }
            else if(relativeAbundance >= 3) {
                return 6;
            }
        }
        else {
            if(relativeAbundance == 2) {
                return 6;
            }
            else if(relativeAbundance >= 3) {
                return 8;
            }
        }
        return 0;
    }

    public int group3Score(int numOfTypes, int relativeAbundance) {
        if(numOfTypes == 0) {
            return 0;
        }
        else if(numOfTypes >= 1 && numOfTypes <= 2) {
            if(relativeAbundance >= 1 && relativeAbundance <= 2) {
                return 2;
            }
            else if(relativeAbundance >= 3) {
                return 4;
            }
        }
        else {
            if(relativeAbundance >= 3) {
                return 6;
            }
        }
        return 0;
    }

    public int group4Score(int numOfTypes, int relativeAbundance) {
        if(numOfTypes == 0) {
            return 0;
        }
        else if(numOfTypes >= 1 && numOfTypes <= 2) {
            if(relativeAbundance >= 1 && relativeAbundance <= 6) {
                return 2;
            }
            else if (relativeAbundance >= 7) {
                return 0;
            }
        }
        else {
            if(relativeAbundance >= 1 && relativeAbundance <= 6) {
                return 4;
            }
            else if (relativeAbundance >= 7) {
                return 0;
            }
        }
        return 0;
    }

    public int group5Score(int num) {
        if(num == 0) {
            return 6;
        }
        else if(num >= 1 && num <= 20) {
            return 2;
        }
        else {
            return 0;
        }
    }
}

package ocow.com.customactionbar;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static ocow.com.customactionbar.R.id.present_btn;

/**
 * Created by dlark on 07/11/2017.
 */

public class CustomListAdapter extends ArrayAdapter<ListViewItemsClass> {

    private int rows;
    private int newHeight;

    public CustomListAdapter(@NonNull Context context, @LayoutRes int resource, int mheight , int rows) {
        super(context, resource);
        this.rows= rows;
        this.newHeight = mheight;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.listview_item, null);

        ImageView iv = (ImageView) v.findViewById(R.id.lv_item);
        TextView tv = (TextView) v.findViewById(R.id.lv_title);
        RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.content);

        int hHeight = (newHeight/rows)-10;
        int tv_height = hHeight/2;

//        ViewGroup.LayoutParams parms = (ViewGroup.LayoutParams) rl.getLayoutParams();
//        parms.height = hHeight;
//        rl.setLayoutParams(parms);
//
//        ViewGroup.LayoutParams parms2 = (ViewGroup.LayoutParams) tv.getLayoutParams();
//        parms2.height = tv_height;
//        tv.setLayoutParams(parms2);

        Button none = (Button) v.findViewById(R.id.none_btn);
        Button present_btn = (Button) v.findViewById(R.id.present_btn);
        Button mod_btn = (Button) v.findViewById(R.id.moderate_btn);
        Button abun_btn = (Button) v.findViewById(R.id.abundant_btn);

        iv.setImageResource(getItem(position).getImage());
        tv.setText(getItem(position).getTitle());

        return v;
    }

    @Nullable
    @Override
    public ListViewItemsClass getItem(int position) {
        return super.getItem(position);
    }


}

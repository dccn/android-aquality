package ocow.com.customactionbar;

import android.text.method.PasswordTransformationMethod;
import android.view.View;

/**
 * Created by dlark on 03/12/2017.
 */

public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
    @Override
    public CharSequence getTransformation(CharSequence source, View view) {
        return new PasswordCharSequence(source);
    }

    private class PasswordCharSequence implements CharSequence {
        private CharSequence mSource;
        public PasswordCharSequence(CharSequence source) {
            mSource = source;
        }
        public int length() {
            return mSource.length();
        }

        @Override
        public char charAt(int i) {
            return '*';
        }

        public CharSequence subSequence(int start, int end) {
            return mSource.subSequence(start,end);
        }
    }
}
//https://stackoverflow.com/questions/14051962/change-edittext-password-mask-character-to-asterisk
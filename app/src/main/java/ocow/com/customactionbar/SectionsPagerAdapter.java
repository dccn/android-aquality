package ocow.com.customactionbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ocow.com.customactionbar.locationforms.Form1;
import ocow.com.customactionbar.locationforms.Form10;
import ocow.com.customactionbar.locationforms.Form11;
import ocow.com.customactionbar.locationforms.Form2;
import ocow.com.customactionbar.locationforms.Form3;
import ocow.com.customactionbar.locationforms.Form4;
import ocow.com.customactionbar.locationforms.Form5;
import ocow.com.customactionbar.locationforms.Form6;
import ocow.com.customactionbar.locationforms.Form7;
import ocow.com.customactionbar.locationforms.Form8;
import ocow.com.customactionbar.locationforms.Form9;
import ocow.com.customactionbar.locationforms.Timer;

/**
 * Created by dlark on 11/12/2017.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                Form1 form1 = new Form1();
                return form1;
            case 1:
                Form2 form2 = new Form2();
                return form2;
            case 2:
                Form3 form3 = new Form3();
                return form3;
            case 3:
                Form4 form4 = new Form4();
                return form4;
            case 4:
                Form5 form5 = new Form5();
                return form5;
            case 5:
                Form6 form6 = new Form6();
                return form6;
            case 6:
                Form7 form7 = new Form7();
                return form7;
            case 7:
                Form8 form8 = new Form8();
                return form8;
            case 8:
                Form9 form9 = new Form9();
                return form9;
            case 9:
                Form10 form10 = new Form10();
                return form10;
            case 10:
                Form11 form11 = new Form11();
                return form11;
            case 11:
                Timer timer = new Timer();
                return timer;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 12;
    }



}

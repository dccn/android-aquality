package ocow.com.customactionbar;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import java.util.ArrayList;

public class TinderOnClick extends SwipeBackActivity {

    private ViewPager vp;
    private TinderGalleryAdapter tGAdapter;
    private TextView tv1, tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tinder_on_click);
        Bundle bundle = getIntent().getExtras();
        int pos = bundle.getInt("pos");

        ArrayList<ArrVals> list = new ArrayList<>();

        tv1 = (TextView) findViewById(R.id.tinder_onclick_title);
        tv2 = (TextView) findViewById(R.id.tinder_onclick_desc);

        if(pos == 0) {
            list.add(new ArrVals(R.drawable.caenis_1));
            list.add(new ArrVals(R.drawable.caenis_2));
            list.add(new ArrVals(R.drawable.caenis_3));
            tv1.setText("Caenis");
            tv2.setText("(Angler͛s Curse) accumulates silt from its habit of silt burrowing.It is one of the smaller mayflies. A poor enough swimmer.  One species (Caenis rivulorum, above) has a distinct banding (black-white). Gill cover overlaps the body.  Size up to 8 mm.");

        }

        vp = (ViewPager) findViewById(R.id.tinder_onclick_pager);
        tGAdapter = new TinderGalleryAdapter(this, list);
        vp.setAdapter(tGAdapter);
        vp.setOffscreenPageLimit(tGAdapter.getCount());


        setDragEdge(SwipeBackLayout.DragEdge.TOP);
    }
}

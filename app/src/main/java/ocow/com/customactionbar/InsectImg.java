package ocow.com.customactionbar;

import android.graphics.drawable.Drawable;

public class InsectImg  {
    private String imageId;
    private byte[] imageByteArray;

    public InsectImg() {

    }

    public InsectImg(String imageId, byte[] imageByteArray) {
        this.imageId = imageId;
        this.imageByteArray = imageByteArray;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public byte[] getImageByteArray() {
        return imageByteArray;
    }

    public void setImageByteArray(byte[] imageByteArray) {
        this.imageByteArray = imageByteArray;
    }
}

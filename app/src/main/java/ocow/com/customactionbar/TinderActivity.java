package ocow.com.customactionbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ocow.com.customactionbar.accounts.LoginActivity;

public class TinderActivity extends ToolbarConfig {

    private SwipeFlingAdapterView flingContainer;
    private TinderAdapter tadapter;
    private int i, group = 0;
    private android.app.ActionBar ab;
    private MainActivity ma;
    private String[] mainArray;
    private String[][] insectArray;
    private ArrayList<InsectClass> insectArray2;
    private int arrIndex = 0;
    String insectNames[];
    private ImageView info_17;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tinder);
        toolBar();

        insectArray2 = new ArrayList<>();

        Bundle b = this.getIntent().getExtras();
        mainArray = b.getStringArray("mainArray");
        group = b.getInt("group");

//        for(int i =0; i < mainArray.length; i++){
//            Log.i("groups",""+mainArray[i]);
//        }


        ma = new MainActivity();
        insectNames = new String[]{"Caenis", "Ecdyonurus", "Ephemera Danica", "Heptagenia", "Rhithrogena", "Amphinemura", "Isoperla", "Lauctra", "Perla", "Protonemura","Goeridae", "Hydropsychidae", "Hydroptilidae", "Limnephlidae", "Polycentropidae", "Rhycophila", "Sericostomatidae",
                    "Chironomus", "Dicranota", "Diseniella", "Diseniella", "Flatworms", "Leeches", "Lumbriculidae", "Lymnaea", "Naididae", "Planorbis", "Potamopyrgus", "Simuildae","Asellus"};

        group = getIntent().getIntExtra("group",0);
        tadapter = new TinderAdapter(getApplicationContext(), 0);

        fillAdapter();

        TextView done = (TextView) findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putStringArray("mainArray", mainArray);

                Intent in = new Intent(TinderActivity.this, ReviewForm.class);
                in.putExtras(b);
                in.putExtra("insectArray",insectArray2);
                startActivity(in);
            }
        });

        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);
        flingContainer.setAdapter(tadapter);

        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                tadapter.remove(tadapter.getItem(0));
                tadapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                int count = tadapter.getCount();
                int pos = 29 - (count+1);
                insectAmount(pos);
                Log.i("Adapter Pos:",""+pos);
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                // Ask for more data here
//                al.add("XML ".concat(String.valueOf(i)));
//                arrayAdapter.notifyDataSetChanged();
                Log.d("LIST", "notified");
                fillAdapter();
                i++;
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
//                makeToast(getApplicationContext(), "Clicked!");
                Intent in = new Intent(TinderActivity.this, TinderOnClick.class);
                in.putExtra("pos",itemPosition);
                startActivity(in);
                overridePendingTransition(R.anim.slide_up,0);
            }
        });

    }
    @Override
    public void toolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.aquality_logo);

//        Toolbar back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        Returns to the previous Activity
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent in = new Intent(TinderActivity.this, GroupsActivity.class);
//                startActivity(in);
                finish();
            }
        });

        setHints();
    }

    public void right(View view) {
        flingContainer.getTopCardListener().selectRight();
    }

    public void left(View view) {
        flingContainer.getTopCardListener().selectLeft();
    }

    public void fillAdapter() {
//        if(group == 1){
            tadapter.add(new TinderClass(R.drawable.caenis_1,"Caenis","",1));
            tadapter.add(new TinderClass(R.drawable.ecdyonurus_1,"Ecdyonurus","",1));
            tadapter.add(new TinderClass(R.drawable.ephemera_danica,"Ephemera Danica","",1));
            tadapter.add(new TinderClass(R.drawable.heptagenia,"Heptagenia","",1));
            tadapter.add(new TinderClass(R.drawable.rhithrogena_1,"Rhithrogena","",1));
//        } else if(group == 2) {
            tadapter.add(new TinderClass(R.drawable.amphinemura,"Amphinemura","",2));
            tadapter.add(new TinderClass(R.drawable.isoperla_1,"Isoperla","",2));
            tadapter.add(new TinderClass(R.drawable.leuctra,"Lauctra","",2));
            tadapter.add(new TinderClass(R.drawable.perla_1,"Perla","",2));
            tadapter.add(new TinderClass(R.drawable.protonemura,"Protonemura","",2));
//        } else if(group == 3){
            tadapter.add(new TinderClass(R.drawable.goeridae, "Goeridae","",3));
            tadapter.add(new TinderClass(R.drawable.hydropsychidae_1, "Hydropsychidae","",3));
            tadapter.add(new TinderClass(R.drawable.hydroptilidae, "Hydroptilidae","",3));
            tadapter.add(new TinderClass(R.drawable.limnephlidae_1, "Limnephlidae","",3));
            tadapter.add(new TinderClass(R.drawable.polycentropidae_1, "Polycentropidae","",3));
            tadapter.add(new TinderClass(R.drawable.rhyacophila_1, "Rhycophila","",3));
            tadapter.add(new TinderClass(R.drawable.sericostomatidae, "Sericostomatidae","",3));
//        } else if(group == 4){
            tadapter.add(new TinderClass(R.drawable.chironomus, "Chironomus","",4));
            tadapter.add(new TinderClass(R.drawable.dicranota, "Dicranota","",4));
            tadapter.add(new TinderClass(R.drawable.diseniella_worm, "Diseniella","Worm",4));
            tadapter.add(new TinderClass(R.drawable.flatworms, "Flatworms","",4));
            tadapter.add(new TinderClass(R.drawable.leeches, "Leeches","",4));
            tadapter.add(new TinderClass(R.drawable.lumbriculidae_worm, "Lumbriculidae","Worm",4));
            tadapter.add(new TinderClass(R.drawable.lymnaea_snail, "Lymnaea","Snail",4));
            tadapter.add(new TinderClass(R.drawable.naididae_worm, "Naididae","Worm",4));
            tadapter.add(new TinderClass(R.drawable.planorbis_snail, "Planorbis","Snail",4));
            tadapter.add(new TinderClass(R.drawable.potamopyrgus_snail, "Potamopyrgus","Snail",4));
            tadapter.add(new TinderClass(R.drawable.simulidae, "Simuildae","",4));
//        } else {
            tadapter.add(new TinderClass(R.drawable.asellus_1, "Asellus","",5));
//        }
    }

    //Alert Dialog for inserting number of insects in sample
    public void insectAmount(final int position) {

        String[] options = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        final ArrayAdapter<String> ad= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, options);

        final Spinner sp = new Spinner(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10,0,10,0);

        sp.setLayoutParams(lp);
        sp.setAdapter(ad);

        final AlertDialog.Builder builder = new AlertDialog.Builder(TinderActivity.this);
        builder.setView(sp);
        builder.setTitle(insectNames[position]);
        builder.setMessage("Please select the estimated number of "+insectNames[position]+" in your sample.");

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("okay buttons", "test");
                // Add value to database / temp database for user review
                String spin = sp.getSelectedItem().toString();
                Log.i("Spinner Val",""+spin);
                submitToArr(position, spin);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("cancel Button", "Test");
            }
        });
        builder.create().show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            // Alert Dialog when exiting form.
            final AlertDialog.Builder builder = new AlertDialog.Builder(TinderActivity.this);
            builder.setTitle("Exiting Sample");
            builder.setMessage("Are you sure you want to exit? You will loose all data entered.");

            builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent in = new Intent(getApplicationContext(), MainMenu.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(in);
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //cancelled
                }
            });
            builder.create().show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void submitToArr(int pos, String spin) {
        if (arrIndex > 28){
            arrIndex = 0;
        }
        if(pos == 0) {
            insectArray2.add(new InsectClass("Caenis",spin, 1));
            arrIndex++;
        } else if (pos == 1){
            insectArray2.add(new InsectClass("Ecdyonurus",spin, 1));
            arrIndex++;
        } else if (pos == 2){
            insectArray2.add(new InsectClass("Ephemera",spin, 1));
            arrIndex++;
        } else if (pos == 3){
            insectArray2.add(new InsectClass("Heptagenia",spin, 1));
            arrIndex++;
//            insectName = "Heptagenia";
        } else if (pos == 4){
            insectArray2.add(new InsectClass("Rhithrogena",spin, 1));
            arrIndex++;
//            insectName = "Rhithrogena";
        } else if (pos == 5){
            insectArray2.add(new InsectClass("Amphinemura",spin, 2));
            arrIndex++;
//            insectName = "Amphinemura";
        } else if (pos == 6){
            insectArray2.add(new InsectClass("Isoperla",spin, 2));
            arrIndex++;
//            insectName = "Isoperla";
        } else if (pos == 7){
            insectArray2.add(new InsectClass("Lauctra",spin, 2));
            arrIndex++;
//            insectName = "Lauctra";
        } else if (pos == 8){
            insectArray2.add(new InsectClass("Perla",spin, 2));
            arrIndex++;
//            insectName = "Perla";
        } else if (pos == 9){
            insectArray2.add(new InsectClass("Protonemura",spin, 2));
            arrIndex++;
//            insectName = "Protonemura";
        } else if (pos == 10){
            insectArray2.add(new InsectClass("Goeridae",spin, 3));
            arrIndex++;
//            insectName = "Goeridae";
        } else if (pos == 11){
            insectArray2.add(new InsectClass("Hydropsychidae",spin, 3));
            arrIndex++;
//            insectName = "Hydropsychidae";
        } else if (pos == 12){
            insectArray2.add(new InsectClass("Hydroptilidae",spin, 3));
            arrIndex++;
//            insectName = "Hydroptilidae";
        } else if (pos == 13){
            insectArray2.add(new InsectClass("Limnephlidae",spin, 3));
            arrIndex++;
//            insectName = "Limnephlidae";
        } else if (pos == 14){
            insectArray2.add(new InsectClass("Polycentropidae",spin, 3));
            arrIndex++;
//            insectName = "Polycentropidae";
        } else if (pos == 15){
            insectArray2.add(new InsectClass("Rhycophila",spin, 3));
            arrIndex++;
//            insectName = "Rhycophila";
        } else if (pos == 16){
            insectArray2.add(new InsectClass("Sericostomatidae",spin, 3));
            arrIndex++;
//            insectName = "Sericostomatidae";
        } else if (pos == 17){
            insectArray2.add(new InsectClass("Chironomus",spin, 4));
            arrIndex++;
//            insectName  = "Chironomus";
        } else if (pos == 18){
            insectArray2.add(new InsectClass("Dicranota",spin, 4));
            arrIndex++;
//            insectName = "Dicranota";
        } else if (pos == 19){
            insectArray2.add(new InsectClass("Diseniella",spin, 4));
            arrIndex++;
//            insectName = "Diseniella";
        } else if (pos == 20){
            insectArray2.add(new InsectClass("Flatworms",spin, 4));
            arrIndex++;
//            insectName = "Flatworms";
        } else if (pos == 21){
            insectArray2.add(new InsectClass("Leeches",spin, 4));
            arrIndex++;
//            insectName = "Leeches";
        } else if (pos == 22){;
            insectArray2.add(new InsectClass("Lumbriculidae",spin, 4));
            arrIndex++;
//            insectName = "Lumbriculidae";
        } else if (pos == 23){
            insectArray2.add(new InsectClass("Lymnaea",spin, 4));
            arrIndex++;
//            insectName = "Lymnaea";
        } else if (pos == 24){
            insectArray2.add(new InsectClass("Naididae",spin, 4));
            arrIndex++;
//            insectName = "Naididae";
        } else if (pos == 25){
            insectArray2.add(new InsectClass("Planorbis",spin, 4));
            arrIndex++;
//            insectName = "Planorbis";
        } else if (pos == 26){
            insectArray2.add(new InsectClass("Potamopyrgus",spin, 4));
            arrIndex++;
//            insectName = "Potamopyrgus";
        } else if (pos == 27){
            insectArray2.add(new InsectClass("Simuildae",spin, 4));
            arrIndex++;
//            insectName = "Simuildae";
        } else if (pos == 28){
            insectArray2.add(new InsectClass("Asellus",spin, 5));
            arrIndex++;
//            insectName = "Asellus";
        }
    }

    public ArrayList<InsectClass> getInsectArray2(){
        return insectArray2;
    }

    public void getHint(String title, String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(mess);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the dialog
            }
        });
        builder.create().show();
    }

    public void setHints() {
        info_17 = (ImageView) findViewById(R.id.info_17);

        info_17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHint("Identifying Insects", "Swipe Left until you see your insect, then swipe right. Once you have swiped right then enter the amount that you have identified.");
            }
        });
    }
}
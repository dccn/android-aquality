package ocow.com.customactionbar;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static android.media.CamcorderProfile.get;
import static android.support.v7.widget.RecyclerView.NO_POSITION;

/**
 * Created by dlark on 12/11/2017.
 */

public class TinderAdapter extends ArrayAdapter<TinderClass> {

    int group;

    public TinderAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    @Nullable
    @Override
    public TinderClass getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(@Nullable TinderClass item) {
        return super.getPosition(item);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        return super.getView(position, convertView, parent);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.tinder_item, parent, false);

        group = getItem(position).getGroup();

        RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.group_colour);
        ImageView iv = (ImageView) convertView.findViewById(R.id.tinder_img);
        TextView tv1 = (TextView) convertView.findViewById(R.id.tinder_title);
        TextView tv2 = (TextView) convertView.findViewById(R.id.tinder_sub_title);

        Log.d("Adapater group",""+group);

        if(group == 1) {
            rl.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.red));
        } else if (group == 2) {
            rl.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.green));
        } else if (group == 3) {
            rl.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.blue));
        } else if (group == 4) {
            rl.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.orange));
        } else if (group == 5) {
            rl.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.purple));
        } else {
            Log.e("Groups Error: ", "Problem retrieving group id for TinderActivity");
        }

        iv.setImageResource(getItem(position).getImg());
        tv1.setText(getItem(position).getTitle());
        tv2.setText(getItem(position).getSub_title());


        return convertView;
    }
}

package ocow.com.customactionbar;

/**
 * Created by dlark on 07/11/2017.
 */

public class ListViewItemsClass {
    public int image;
    public String title;

    public ListViewItemsClass(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

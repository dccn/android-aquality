package ocow.com.customactionbar;

import android.graphics.drawable.Drawable;

/**
 * Created by dlark on 12/11/2017.
 */

public class TinderClass {
    private int img;
    private int group;
    private String title, sub_title;

    public TinderClass(int img, String title, String sub_title, int group) {
        this.img = img;
        this.title = title;
        this.sub_title = sub_title;
        this.group = group;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }
}
